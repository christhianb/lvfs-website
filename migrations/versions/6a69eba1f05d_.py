# Custom template
"""empty message

Revision ID: 6a69eba1f05d
Revises: 8c5b86d6c9d1
Create Date: 2023-05-05 11:23:49.806813

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "6a69eba1f05d"
down_revision = "8c5b86d6c9d1"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("firmware_revisions", schema=None) as batch_op:
        batch_op.alter_column(
            "checksum_sha1", existing_type=sa.VARCHAR(length=40), nullable=False
        )
        batch_op.alter_column(
            "checksum_sha256", existing_type=sa.VARCHAR(length=64), nullable=False
        )


def downgrade():
    with op.batch_alter_table("firmware_revisions", schema=None) as batch_op:
        batch_op.alter_column(
            "checksum_sha256", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "checksum_sha1", existing_type=sa.VARCHAR(length=40), nullable=True
        )
