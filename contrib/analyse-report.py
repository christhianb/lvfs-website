#!/usr/bin/python3
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import json
import sys
import csv
from typing import Any


class Report2:
    def __init__(self) -> None:
        self.pcr0_sha1 = None
        self.pcr0_sha256 = None
        self.version = None
        self.host_vendor = None
        self.host_family = None
        self.host_product = None

    def __lt__(self, other: Any) -> bool:
        return self.pcr0_sha1 < other.pcr0_sha1

    def __eq__(self, other: Any) -> bool:
        return self.pcr0_sha1 == other.pcr0_sha1

    @property
    def __dict__(self) -> dict[str, str]:  # type: ignore
        val: dict[str, str] = {}
        if self.pcr0_sha1:
            val["Pcr0_SHA1"] = self.pcr0_sha1
        if self.pcr0_sha256:
            val["Pcr0_SHA256"] = self.pcr0_sha256
        if self.version:
            val["VersionNew"] = self.version
        if self.host_vendor:
            val["HostVendor"] = self.host_vendor
        if self.host_family:
            val["HostFamily"] = self.host_family
        if self.host_product:
            val["HostProduct"] = self.host_product
        return val

    @property
    def key(self):
        return f"{self.pcr0_sha1}:{self.host_vendor}:{self.host_family}:{self.host_product}"


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(f"Usage: {sys.argv[0]} export.json export.csv")
        sys.exit(1)
    with open(sys.argv[1], "rb") as f:
        data = json.load(f)

    rprts: dict[str, Report2] = {}

    print(f"Number of reports: {len(data)}")
    for report in data:
        if report["Plugin"] not in ["uefi_capsule", "uefi"]:
            continue

        rprt = Report2()
        rprt.pcr0_sha1 = report.get("Pcr0_SHA1")
        if not rprt.pcr0_sha1:
            rprt.pcr0_sha1 = report.get("ChecksumDevice")
        if not rprt.pcr0_sha1:
            continue
        rprt.pcr0_sha256 = report.get("Pcr0_SHA256")
        rprt.version = report.get("VersionNew")
        rprt.host_vendor = report.get("HostVendor")
        rprt.host_family = report.get("HostFamily")
        rprt.host_product = report.get("HostProduct")
        if rprt.key not in rprts:
            rprts[rprt.key] = rprt

    print(f"Number of unique reports: {len(rprts)}")

    with open(sys.argv[2], mode="wb") as csv_file:
        fieldnames = [
            "Pcr0_SHA1",
            "Pcr0_SHA256",
            "VersionNew",
            "HostVendor",
            "HostFamily",
            "HostProduct",
        ]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)  # type: ignore
        writer.writeheader()
        for rprt in sorted(rprts.values()):
            writer.writerow(rprt.__dict__)
