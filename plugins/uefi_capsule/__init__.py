#!/usr/bin/python3
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Optional
import struct
import uuid

from lvfs.pluginloader import PluginBase, PluginSettingBool
from lvfs.tests.models import Test
from lvfs.firmware.models import Firmware
from lvfs.components.models import Component

CAPSULE_FLAGS_PERSIST_ACROSS_RESET = 0x00010000
CAPSULE_FLAGS_POPULATE_SYSTEM_TABLE = 0x00020000
CAPSULE_FLAGS_INITIATE_RESET = 0x00040000


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "uefi-capsule")
        self.name = "UEFI Capsule"
        self.summary = "Check the UEFI capsule header and file structure"
        self.settings.append(
            PluginSettingBool(
                key="uefi_capsule_check_header", name="Enabled", default=True
            )
        )

    def require_test_for_md(self, md: Component) -> bool:
        # only run for capsule updates
        if not md.protocol:
            return False
        return md.protocol.value == "org.uefi.capsule"

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def run_test_on_md(self, test: Test, md: Component) -> None:
        # unpack the header
        blob: Optional[bytes] = md.read()
        if not blob:
            return
        try:
            data = struct.unpack("<16sIII", blob[:28])
        except struct.error:
            test.add_fail("FileSize", f"0x{len(blob):x}")
            # we have to abort here, no further tests are possible
            return

        # check the GUID
        guid = str(uuid.UUID(bytes_le=data[0]))
        if guid == "00000000-0000-0000-0000-000000000000":
            test.add_fail("GUID", f"{guid} is not valid")
        elif guid in [
            "3b6686bd-0d76-4030-b70e-b5519e2fc5a0",  # EFI_CAPSULE_GUID
            "4a3ca68b-7723-48fb-803d-578cc1fec44d",  # EFI_SIGNED_CAPSULE_GUID
        ]:
            test.add_fail(
                "GUID",
                f"{guid} is the hardcoded EFI_CAPSULE_GUID when it is supposed to "
                "be the GUID found in the ESRT table",
            )
        elif guid in ["6dcbd5ed-e82d-4c44-bda1-7194199ad92a"]:  # EFI_FMP_CAPSULE_GUID
            test.add_pass("GUID", f"{guid} (FMP)")
        else:
            referenced_guids = [gu.value for gu in md.guids]
            if guid in referenced_guids:
                test.add_pass("GUID", guid)
            else:
                test.add_fail(
                    "GUID",
                    f"{guid} not found in {','.join(referenced_guids)}",
                )

        # check the header size
        if data[1] == 0:
            test.add_fail("HeaderSize", f"0x{data[1]:x}")
        else:
            test.add_pass("HeaderSize", f"0x{data[1]:x}")

        # check if the flags are sane
        if (
            data[2] & CAPSULE_FLAGS_POPULATE_SYSTEM_TABLE > 0
            and data[2] & CAPSULE_FLAGS_PERSIST_ACROSS_RESET == 0
        ):
            test.add_warn(
                "Flags",
                f"0x{data[2]:x} -- POPULATE_SYSTEM_TABLE requires PERSIST_ACROSS_RESET",
            )
        elif (
            data[2] & CAPSULE_FLAGS_INITIATE_RESET > 0
            and data[2] & CAPSULE_FLAGS_PERSIST_ACROSS_RESET == 0
        ):
            test.add_warn(
                "Flags",
                f"0x{data[2]:x} -- INITIATE_RESET requires PERSIST_ACROSS_RESET",
            )
        else:
            test.add_pass("Flags", f"0x{data[2]:x}")

        # check the capsule image size
        if data[3] == len(blob):
            test.add_pass("CapsuleImageSize", f"0x{data[3]:x}")
        else:
            test.add_warn(
                "CapsuleImageSize",
                f"0x{data[3]:x} does not match file size 0x{len(blob):x}",
            )
