#!/usr/bin/python3
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-member,too-few-public-methods

import os
import glob
from typing import Any, Optional

import yara
from sqlalchemy.exc import NoResultFound

from lvfs import db
from lvfs.pluginloader import PluginBase
from lvfs.pluginloader import PluginSettingBool, PluginSettingList
from lvfs.tests.models import Test
from lvfs.claims.models import Claim
from lvfs.components.models import Component
from lvfs.firmware.models import Firmware


def _run_on_blob(self: Any, test: Test, md: Component, title: str, blob: bytes) -> None:
    matches = self.rules.match(data=blob)
    for match in matches:
        # do what we can
        description = None
        if "description" in match.meta:
            description = match.meta["description"].replace("\0", "")

        if "fail" not in match.meta or match.meta["fail"]:
            msg = f"{match.rule} YARA test failed"
            for string in match.strings:
                if len(string) == 3:
                    try:
                        msg += f": found {string[2].decode()}"
                    except UnicodeDecodeError:
                        pass
            if description:
                msg += f": {description}"
            test.add_fail(title, msg)
        elif "claim" in match.meta:
            try:
                claim = (
                    db.session.query(Claim)
                    .filter(Claim.kind == match.meta["claim"])
                    .one()
                )
            except NoResultFound:
                test.add_warn("YARA", f"Failed to find claim: {match.meta['claim']}")
                continue
            md.add_claim(claim)


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "blocklist")
        self.rules = None
        self.name = "Blocklist"
        self.summary = "Use YARA to check firmware for problems"
        self.order_after = ["uefi-extract", "intelme"]
        self.settings.append(
            PluginSettingBool(key="blocklist_enabled", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSettingList(
                key="blocklist_dirs",
                name="Rule Directories",
                default=["plugins/blocklist/rules"],
            )
        )

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def run_test_on_md(self, test: Test, md: Component) -> None:
        # compile the list of rules
        if not self.rules:
            fns: list[str] = []
            for value in self.get_setting_list("blocklist_dirs", required=True):
                fns.extend(glob.glob(os.path.join(value, "*.yar")))
            if not fns:
                test.add_pass("No YARA rules to use")
                return
            filepaths: dict[str, str] = {}
            for fn in fns:
                filepaths[os.path.basename(fn)] = fn
            try:
                self.rules = yara.compile(filepaths=filepaths)
            except yara.SyntaxError as e:
                test.add_fail("YARA", f"Failed to compile rules: {e!s}")
                return

        # run analysis on the component and any shards
        blob: Optional[bytes] = md.read()
        if blob:
            _run_on_blob(self, test, md, md.filename_contents, blob)
        for shard in md.shards:
            blob = shard.read()
            if blob:
                _run_on_blob(self, test, md, shard.name, blob)


# run with PYTHONPATH=. ./env/bin/python3 plugins/blocklist/__init__.py
if __name__ == "__main__":

    from lvfs import create_app

    with create_app().app_context():
        plugin = Plugin()
        _test = Test(plugin_id=plugin.id)
        _fw = Firmware()
        _md = Component()
        _fw.mds.append(_md)
        _md.write(b"CN=DO NOT TRUST - AMI Test PK")
        plugin.run_test_on_md(_test, _md)
        for attribute in _test.attributes:
            print(attribute)
