#!/usr/bin/python3
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Optional
import struct
import zlib

from lvfs.pluginloader import PluginBase, PluginSettingBool
from lvfs.tests.models import Test
from lvfs.components.models import Component
from lvfs.firmware.models import Firmware


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "dfu")
        self.name = "DFU"
        self.summary = "Check the DFU firmware footer"
        self.settings.append(
            PluginSettingBool(key="dfu_check_footer", name="Enabled", default=True)
        )

    def require_test_for_md(self, md: Component) -> bool:
        if not md.protocol:
            return False
        return md.protocol.value == "org.usb.dfu"

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def run_test_on_md(self, test: Test, md: Component) -> None:
        # unpack the footer
        blob: Optional[bytes] = md.read()
        if not blob:
            return
        sz = len(blob)
        if sz < 16:
            test.add_fail("FileSize", f"0x{sz:x}")
            # we have to abort here, no further tests are possible
            return

        footer = blob[sz - 16 :]
        try:
            (
                bcdDevice,
                idProduct,
                idVendor,
                bcdDFU,
                ucDfuSig,
                bLength,
                dwCRC,
            ) = struct.unpack("<HHHH3sBL", footer)
        except struct.error:
            test.add_fail("Footer", str(footer))
            # we have to abort here, no further tests are possible
            return

        # check ucDfuSig
        if ucDfuSig != b"UFD":
            test.add_fail("Footer Signature", f"{ucDfuSig} is not valid")
            # we have to abort here, no further tests are possible
            return

        # check bcdDevice
        if bcdDevice in [0x0000, 0xFFFF]:
            test.add_warn("Device", f"0x{bcdDevice:04x} is not valid")

        # check idProduct
        if idProduct in [0x0000, 0xFFFF]:
            test.add_warn("Product", f"0x{idProduct:04x} is not valid")

        # check idVendor
        if idVendor == 0x0000:
            test.add_warn("Vendor", f"0x{idVendor:04x} is not valid")

        # check bcdDFU
        if bcdDFU in [0x0101, 0x0100]:
            test.add_pass("DFU Version", f"0x{bcdDFU:04x}")
        else:
            test.add_warn("DFU Version", f"0x{bcdDFU:04x} is not valid")

        # check bLength
        if bLength >= 10:
            test.add_pass("DFU Length", f"0x{bLength:02x}")
        else:
            test.add_fail("DFU Length", f"0x{bLength:02x} is not valid")

        # check dwCRC
        crc_correct = zlib.crc32(blob[: sz - 4]) ^ 0xFFFFFFFF
        if dwCRC == crc_correct:
            test.add_pass("CRC", f"0x{dwCRC:04x}")
        else:
            test.add_fail(
                "CRC",
                f"0x{dwCRC:04x} invalid, expected 0x{crc_correct:04x}",
            )


# run with PYTHONPATH=. ./env/bin/python3 plugins/dfu/__init__.py
if __name__ == "__main__":
    import sys

    from lvfs.protocols.models import Protocol

    from lvfs import create_app

    with create_app().app_context():
        for _argv in sys.argv[1:]:
            print("Processing", _argv)
            plugin = Plugin()
            _test = Test(plugin_id=plugin.id)
            _fw = Firmware()
            _md = Component()
            _md.component_id = 999999
            _md.filename_contents = "filename.bin"
            _md.protocol = Protocol(value="org.usb.dfu")
            _fw.mds.append(_md)
            with open(_argv, "rb") as _f:
                _md.write(_f.read())
            plugin.run_test_on_md(_test, _md)
            for attribute in _test.attributes:
                print(attribute)
