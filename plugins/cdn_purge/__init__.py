#!/usr/bin/python3
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=line-too-long

import os
import fnmatch
import json

import requests

from lvfs.pluginloader import PluginBase, PluginError
from lvfs.pluginloader import PluginSetting, PluginSettingBool, PluginSettingList


def _basename_matches_globs(basename: str, globs: list[str]) -> bool:
    for glob in globs:
        if fnmatch.fnmatch(basename, glob):
            return True
    return False


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "cdn-purge")
        self.name = "CDN Purge"
        self.summary = "Manually purge files from a content delivery network"
        self.settings.append(PluginSettingBool(key="cdn_purge_enable", name="Enabled"))
        self.settings.append(
            PluginSetting(
                key="cdn_purge_uri",
                name="URI",
                default="https://bunnycdn.com/api/purge?url=https://lvfs.b-cdn.net/downloads/",
            )
        )
        self.settings.append(PluginSetting(key="cdn_purge_accesskey", name="Accesskey"))
        self.settings.append(
            PluginSettingList(
                key="cdn_purge_files",
                name="File Whitelist",
                default=["*.xml.?z", "*.xml.?z.*", "*.jcat"],
            )
        )
        self.settings.append(
            PluginSetting(key="cdn_purge_method", name="Request method", default="GET")
        )

    def file_modified(self, fn):
        # is the file in the whitelist
        fns = self.get_setting_list("cdn_purge_files", required=True)
        basename = os.path.basename(fn)
        if not _basename_matches_globs(basename, fns):
            print(f"{basename} not in {fns}")
            return

        # purge
        url = self.get_setting("cdn_purge_uri", required=True) + basename
        headers: dict[str, str] = {}
        accesskey = self.get_setting("cdn_purge_accesskey")
        if accesskey:
            headers["AccessKey"] = accesskey
        try:
            r = requests.request(
                self.get_setting("cdn_purge_method", required=True),
                url,
                headers=headers,
                timeout=10,
            )
        except requests.exceptions.ReadTimeout as e:
            raise PluginError(f"Timeout from {url}") from e
        if r.text:
            try:
                response = json.loads(r.text)
                if response["status"] != "ok":
                    raise PluginError("Failed to purge metadata on CDN: " + r.text)
            except ValueError as e:
                # BunnyCDN doesn't sent a JSON blob
                raise PluginError(f"Failed to purge metadata on CDN: {r.text}") from e
