#!/usr/bin/python3
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,protected-access

import datetime
from typing import Optional, Any

from flask import g

from sqlalchemy import Column, Integer, Text, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship

from lvfs import db

from lvfs.users.models import User
from lvfs.vendors.models import Vendor

REPORT_ATTR_MAP = {
    "AppstreamGlibVersion": "CompileVersion(org.freedesktop.appstream-glib)",
    "ColorhugVersion": "CompileVersion(com.hughski.colorhug)",
    "EfivarVersion": "CompileVersion(com.redhat.efivar)",
    "FwupdateVersion": "CompileVersion(com.redhat.fwupdate)",
    "FwupdVersion": "CompileVersion(org.freedesktop.fwupd)",
    "GUsbVersion": "CompileVersion(org.freedesktop.gusb)",
    "LibsmbiosVersion": "RuntimeVersion(com.dell.libsmbios)",
    "ESPMountPoint": "EspPath",
}


class ReportAttribute(db.Model):  # type: ignore
    __tablename__ = "report_attributes"

    report_attribute_id = Column(Integer, primary_key=True)
    report_id = Column(
        Integer, ForeignKey("reports.report_id"), nullable=False, index=True
    )
    key = Column(Text, nullable=False)
    value = Column(Text, default=None)

    report = relationship("Report", back_populates="attributes")

    def __lt__(self, other: Any) -> bool:
        return self.key < other.key

    def __repr__(self) -> str:
        return f"ReportAttribute({self.key},{self.value})"


def _parse_system_integrity(val: str) -> dict[str, str]:
    new_dict: dict[str, str] = {}
    for line in val.split("\n"):
        try:
            key, value = line.split("=", maxsplit=2)
            new_dict[key] = value
        except ValueError:
            pass
    return new_dict


class Report(db.Model):  # type: ignore
    __tablename__ = "reports"

    report_id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    state = Column(Integer, default=0)
    machine_id = Column(String(64), nullable=False)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), nullable=False, index=True
    )
    checksum = Column(String(64), nullable=False)  # remove?
    issue_id = Column(Integer, default=0)
    user_id = Column(Integer, ForeignKey("users.user_id"), default=None)

    fw = relationship("Firmware", foreign_keys=[firmware_id])
    user = relationship("User", foreign_keys=[user_id])
    attributes = relationship(
        "ReportAttribute",
        back_populates="report",
        lazy="joined",
        cascade="all,delete,delete-orphan",
    )

    @property
    def system_integrity(
        self,
    ) -> Optional[dict[str, tuple[Optional[str], Optional[str]]]]:
        # does the report have both?
        old: Optional[ReportAttribute] = self.get_attribute_by_key("SystemIntegrityOld")
        if not old or not old.value:
            return None
        new: Optional[ReportAttribute] = self.get_attribute_by_key("SystemIntegrityNew")
        if not new or not new.value:
            return None

        # look for differences in old and new
        com_dict: dict[str, tuple[Optional[str], Optional[str]]] = {}
        old_dict: dict[str, str] = _parse_system_integrity(old.value)
        new_dict: dict[str, str] = _parse_system_integrity(new.value)
        for key, old_value in old_dict.items():
            new_value2 = new_dict.get(key)
            if old_value != new_value2:
                com_dict[key] = (old_value, new_value2)
        for key, new_value in new_dict.items():
            old_value2 = old_dict.get(key)
            if not old_value2 and old_value2 != new_value:
                com_dict[key] = (old_value2, new_value)

        # no entries
        if not com_dict:
            return None
        return com_dict

    @property
    def verified_vendor(self) -> Optional[Vendor]:
        # no vendor at all
        if not self.user:
            return None

        # ODM or OEM did testing
        for vendor in self.fw.odm_vendors:
            if self.user.vendor == vendor:
                return vendor

        # unrelated QA team
        return self.user.vendor

    @property
    def verified_host(self) -> str:
        description_list: list[str] = []
        attr_product_key: str = "HostProduct"
        attr = self.get_attribute_by_key("HostVendor")
        if attr and attr.value:
            description_list.append(attr.value)
            if attr.value == "LENOVO":
                attr_product_key = "HostFamily"
        attr = self.get_attribute_by_key(attr_product_key)
        if attr and attr.value:
            description_list.append(attr.value)
        return " ".join(description_list)

    @property
    def verified_description(self) -> Optional[str]:
        description_list: list[str] = []

        # distro version
        attr = self.get_attribute_by_key("DistroId")
        if attr and attr.value:
            distro_names = {
                "arch": "Arch",
                "debian": "Debian",
                "fedora": "Fedora",
                "manjaro": "Manjaro",
                "pop": "Pop!_OS",
                "rhel": "RHEL",
                "sled": "SLED",
                "ubuntu": "Ubuntu",
                "chromeos": "ChromeOS",
                "macos": "macOS",
            }
            distro_version = distro_names.get(attr.value, attr.value)
            attr_ver = self.get_attribute_by_key("DistroVersion")
            if attr_ver and attr_ver.value:
                description_list.append(f"{distro_version} {attr_ver.value}")
            else:
                description_list.append(distro_version)

        # fwupd version
        attr_ver = self.get_attribute_by_key("RuntimeVersion(org.freedesktop.fwupd)")
        if not attr_ver:
            attr_ver = self.get_attribute_by_key("FwupdVersion")
        if attr_ver and attr_ver.value:
            description_list.append(f"fwupd v{attr_ver.value}")

        # a peripheral update so add the host information
        if not self.fw.md_prio.category.matches(
            ["X-System", "X-EmbeddedController", "X-ManagementEngine"]
        ):
            verified_host = self.verified_host
            if verified_host:
                description_list.append(verified_host)

        # simple string
        return "，".join(description_list)

    @property
    def color(self) -> str:
        if self.state == 1:
            return "info"
        if self.state == 2:
            return "success"
        if self.state == 3:
            if self.issue_id:
                return "info"
            return "danger"
        if self.state == 4:
            return "info"
        return "danger"

    def get_attribute_by_key(self, key: str) -> Optional[ReportAttribute]:
        for attr in self.attributes:
            if attr.key == key:
                return attr
        return None

    def to_dict(self) -> dict[str, str]:
        data: dict[str, str] = {}
        if self.user:
            data["Uploader"] = self.user.vendor.display_name
        if self.state == 1:
            data["UpdateState"] = "pending"
        elif self.state == 2:
            data["UpdateState"] = "success"
        elif self.state == 3:
            data["UpdateState"] = "failed"
        elif self.state == 4:
            data["UpdateState"] = "needs-reboot"
        else:
            data["UpdateState"] = "unknown"
        if self.machine_id:
            data["MachineId"] = self.machine_id
        if self.firmware_id:
            data["FirmwareId"] = self.firmware_id
        for attr in self.attributes:
            data[attr.key] = attr.value
        return data

    def check_acl(self, action: str, user: Optional[User] = None) -> bool:
        # fall back
        if not user:
            user = g.user
        if not user:
            return False
        if user.check_acl("@admin"):
            return True

        # depends on the action requested
        if action == "@delete":
            # only admin
            return False
        if action == "@view":
            # QA user can modify any issues matching vendor_id
            if user.check_acl("@qa") and self.fw._is_vendor(user):
                return True
            return False
        raise NotImplementedError(f"unknown security check action: {self}:{action}")

    def __repr__(self) -> str:
        return f"Report({self.report_id})"
