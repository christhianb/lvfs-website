#!/usr/bin/python3
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=unused-argument

import datetime
from typing import Optional, Tuple
import json

from flask import render_template
from flask import current_app as app

from lvfs import db

from lvfs.emails import send_email_sync
from lvfs.components.models import ComponentChecksum
from lvfs.issues.models import Issue
from lvfs.tests.models import Test
from lvfs.firmware.models import (
    FirmwareEvent,
    FirmwareEventKind,
    Firmware,
    FirmwareRevision,
)
from lvfs.metadata.models import Remote
from lvfs.users.models import User
from lvfs.tasks.models import Task
from lvfs.hash import _is_sha1, _is_sha256, _is_sha384

from .models import Report, ReportAttribute, REPORT_ATTR_MAP


def _ensure_system_integrity_test_failure(
    fw: Firmware, system_integrity: dict[str, tuple[Optional[str], Optional[str]]]
) -> None:
    # build a string
    msg: list[str] = []
    for key in system_integrity:
        old, new = system_integrity[key]
        if old and new:
            msg.append(f"{key} changed from {old} to {new}")
        elif old:
            msg.append(f"{key} was removed, old value {old}")
        elif new:
            msg.append(f"{key} was added, new value {new}")

    test: Optional[Test] = fw.find_test_by_plugin_id("system_integrity")
    if not test:
        test = Test(
            plugin_id="system_integrity",
            waivable=True,
            started_ts=datetime.datetime.utcnow(),
            ended_ts=datetime.datetime.utcnow(),
        )
        fw.tests.append(test)
    test.add_fail("System integrity changed", "\n".join(msg))


def _find_issue_for_report_data(data: dict, fw: Firmware) -> Optional[Issue]:
    for issue in db.session.query(Issue).order_by(Issue.priority.desc()):
        if not issue.enabled:
            continue
        if issue.vendor_id not in (1, fw.vendor_id):
            continue
        if issue.matches(data):
            return issue
    return None


def process_report_data(
    payload: str, user: Optional[User] = None
) -> Tuple[list[str], list[str]]:
    """Upload a report"""

    msgs: list[str] = []
    uris: list[str] = []

    # parse JSON data
    try:
        item = json.loads(payload)
    except ValueError as e:
        raise ValueError("No JSON object could be decoded") from e

    # check we got enough data
    for key in ["ReportVersion", "MachineId", "Reports", "Metadata"]:
        if key not in item:
            raise ValueError(f"invalid data, expected {key}")
        if item[key] is None:
            raise ValueError(f"missing data, expected {key}")

    # parse only this version
    if item["ReportVersion"] != 2:
        raise ValueError("report version not supported")

    # add each firmware report
    machine_id = item["MachineId"]
    reports = item["Reports"]
    if len(reports) == 0:
        raise ValueError("no reports included")
    metadata = item["Metadata"]
    if len(metadata) == 0:
        raise ValueError("no metadata included")

    for report in reports:
        for key in ["Checksum", "UpdateState", "Metadata"]:
            if key not in report:
                raise ValueError(f"invalid data, expected {key}")
            if report[key] is None:
                raise ValueError(f"missing data, expected {key}")

        # flattern the report including the per-machine and per-report metadata
        data = metadata
        for key in report:
            # don't store some data
            if key in [
                "Created",
                "Modified",
                "BootTime",
                "UpdateState",
                "DeviceId",
                "UpdateState",
                "DeviceId",
                "Checksum",
            ]:
                continue
            if not _report_key_valid(key):
                continue
            if key == "Metadata":
                md = report[key]
                for md_key in md:
                    if not _report_key_valid(md_key):
                        continue
                    data[md_key] = md[md_key]
                continue

            # do not use deprecated names
            key_safe = REPORT_ATTR_MAP.get(key, key)

            # allow array of strings for any of the keys
            if isinstance(report[key], list):
                data[key_safe] = ",".join(report[key])
            else:
                data[key_safe] = report[key]

        # try to find the checksum (which might not exist on this server)
        fw = (
            db.session.query(Firmware)
            .join(FirmwareRevision)
            .filter(FirmwareRevision.checksum_sha1 == report["Checksum"])
            .with_for_update(of=Firmware)
            .first()
        )
        if not fw:
            fw = (
                db.session.query(Firmware)
                .join(FirmwareRevision)
                .filter(FirmwareRevision.checksum_sha256 == report["Checksum"])
                .with_for_update(of=Firmware)
                .first()
            )
        if not fw:
            msgs.append(
                f"{report['Checksum']} did not match any known firmware archive"
            )
            continue

        # update the device checksums if there is only one component
        if (
            user
            and user.check_acl("@qa")
            and "ChecksumDevice" in data
            and len(fw.mds) == 1
        ):
            md = fw.md_prio
            found = False

            # fwupd v1.2.6 sends an array of strings, before that just a string
            checksums_maybelist = data["ChecksumDevice"]
            if isinstance(checksums_maybelist, list):
                checksums = checksums_maybelist
            else:
                checksums = [checksums_maybelist]

            # does the submitted checksum already exist as a device checksum
            for checksum in checksums:
                for csum in md.device_checksums:
                    if csum.value == checksum:
                        found = True
                        break
                if found:
                    continue
                if _is_sha1(checksum):
                    md.device_checksums.append(
                        ComponentChecksum(value=checksum, kind="SHA1")
                    )
                elif _is_sha256(checksum):
                    md.device_checksums.append(
                        ComponentChecksum(value=checksum, kind="SHA256")
                    )
                elif _is_sha384(checksum):
                    md.device_checksums.append(
                        ComponentChecksum(value=checksum, kind="SHA384")
                    )

        # find any matching report
        issue_id = 0
        if report["UpdateState"] == 3:
            issue = _find_issue_for_report_data(data, fw)
            if issue:
                issue_id = issue.issue_id
                msgs.append("The failure is a known issue")
                uris.append(issue.url)

        # update any old report
        r = (
            db.session.query(Report)
            .filter(Report.checksum == report["Checksum"])
            .filter(Report.machine_id == machine_id)
            .with_for_update(of=Report)
            .first()
        )
        if r:
            r.timestamp = datetime.datetime.utcnow()
            r.state = report["UpdateState"]
            r.user_id = None
            for attr in r.attributes:
                db.session.delete(attr)
            msgs.append(f"{r.checksum} replaces old report")
        else:
            # save a new report in the database
            r = Report(
                machine_id=machine_id,
                firmware_id=fw.firmware_id,
                issue_id=issue_id,
                state=report["UpdateState"],
                checksum=report["Checksum"],
            )
            msgs.append(f"{r.checksum} created as new report")

        # update the firmware so that the QA user does not have to wait 24h
        if r.state == 2:
            fw.report_success_cnt += 1
        elif r.state == 3:
            if r.issue_id:
                fw.report_issue_cnt += 1
            else:
                fw.report_failure_cnt += 1

        # update the LVFS user
        if user:
            r.user_id = user.user_id

        # save all the report entries
        for key in data:
            r.attributes.append(ReportAttribute(key=key, value=data[key]))
        db.session.add(r)

        # does the report indicate that state has changed
        if r.system_integrity:
            _ensure_system_integrity_test_failure(fw, r.system_integrity)

    # all done
    db.session.commit()
    return (msgs, uris)


def _report_key_valid(key: str) -> bool:
    if key.startswith("CompileVersion"):
        return True
    if key.startswith("RuntimeVersion"):
        return True
    return key.find(".") == -1


def _demote_back_to_testing(fw: Firmware, task: Task) -> None:
    # only on the production instance
    if app.config["RELEASE"] != "production":
        return

    # from the server admin
    user = db.session.query(User).filter(User.username == "anon@fwupd.org").first()
    if not user:
        return

    # send email to uploading user
    if fw.user.get_action("notify-demote-failures"):
        send_email_sync(
            "[LVFS] Firmware has been demoted",
            fw.user.email_address,
            render_template("email-firmware-demote.txt", user=fw.user, fw=fw),
            task=task,
        )

    # asynchronously sign straight away, even public remotes
    remote = db.session.query(Remote).filter(Remote.name == "testing").first()
    fw.events.append(
        FirmwareEvent(
            kind=FirmwareEventKind.DEMOTED.value,
            remote_old=fw.remote,
            remote=remote,
            user=user,
        )
    )
    fw.remote = remote
    fw.invalidate_metadata()
    task.add_fail(
        f"Demoted firmware {fw.firmware_id}",
        f"Reported success {fw.success}%",
    )
    db.session.commit()


def _generate_stats_firmware_reports(fw: Firmware, task: Task) -> None:
    # count how many times any of the firmware files were downloaded
    reports_success = 0
    reports_failure = 0
    reports_issue = 0
    for r in db.session.query(Report).filter(
        Report.firmware_id == fw.firmware_id,
        Report.timestamp > datetime.date.today() - datetime.timedelta(weeks=26),
    ):
        if r.state == 2:
            reports_success += 1
        if r.state == 3:
            if r.issue_id:
                reports_issue += 1
            else:
                reports_failure += 1

    # update
    fw.report_success_cnt = reports_success
    fw.report_failure_cnt = reports_failure
    fw.report_issue_cnt = reports_issue

    # check the limits and demote back to embargo if required
    if fw.remote.name == "stable" and fw.is_failure:
        _demote_back_to_testing(fw, task=task)


def _regenerate_reports(task: Task) -> None:
    # update FirmwareReport counts
    for (firmware_id,) in (
        db.session.query(Firmware.firmware_id)
        .join(Remote)
        .filter(Remote.name != "deleted")
        .order_by(Firmware.firmware_id.asc())
    ):
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
        _generate_stats_firmware_reports(fw, task=task)
    db.session.commit()


def task_regenerate(task: Task) -> None:
    _regenerate_reports(task)
