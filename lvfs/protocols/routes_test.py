#!/usr/bin/python3
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_protocol(self, _app, client):
        self.login()
        self.upload()
        rv = client.get("/lvfs/protocols/")
        assert "com.acme" not in rv.data.decode("utf-8"), rv.data

        # create
        rv = client.post(
            "/lvfs/protocols/create",
            data=dict(
                value="com.acme",
            ),
            follow_redirects=True,
        )
        assert b"Added protocol" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/protocols/")
        assert "com.acme" in rv.data.decode("utf-8"), rv.data.decode()
        rv = client.post(
            "/lvfs/protocols/create",
            data=dict(
                value="com.acme",
            ),
            follow_redirects=True,
        )
        assert b"already exists" in rv.data, rv.data.decode()

        # modify
        rv = client.post(
            "/lvfs/protocols/5/modify",
            data=dict(
                value="org.acme",
                name="ACME",
                icon="battery",
                is_signed=True,
                is_transport=True,
            ),
            follow_redirects=True,
        )
        assert b"Modified protocol" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/protocols/")
        assert "org.acme" in rv.data.decode("utf-8"), rv.data.decode()
        assert "ACME" in rv.data.decode("utf-8"), rv.data.decode()
        assert "battery" in rv.data.decode("utf-8"), rv.data.decode()

        # create a flag
        rv = client.post(
            "/lvfs/protocols/1/flag/create",
            data={"value": "ca3"},
            follow_redirects=True,
        )
        assert b"Added flag" in rv.data, rv.data.decode()

        # show the flags page
        rv = client.get("/lvfs/protocols/1/flag/list")
        assert b"ca3" in rv.data, rv.data.decode()

        # delete a flag
        rv = client.post("/lvfs/protocols/1/flag/1/delete", follow_redirects=True)
        assert b"Deleted flag" in rv.data, rv.data.decode()
        assert b"ca3" not in rv.data, rv.data.decode()

        # delete
        rv = client.post("/lvfs/protocols/5/delete", follow_redirects=True)
        assert b"Deleted protocol" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/protocols/")
        assert "com.acme" not in rv.data.decode("utf-8"), rv.data.decode()


if __name__ == "__main__":
    unittest.main()
