#!/usr/bin/python3
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=unused-argument

import gzip
import os
import zipfile
import io
import csv
from collections import defaultdict
from io import StringIO
from flask import current_app as app

import requests

from lvfs import db

from lvfs.tasks.models import Task

from .models import Geoip


def _convert_ip_addr_to_integer(ip_addr: str) -> int:
    # ipv4
    if ip_addr.find(".") != -1:
        try:
            ip_vals = ip_addr.split(".")
            return (
                int(ip_vals[0]) * 0x1000000
                + int(ip_vals[1]) * 0x10000
                + int(ip_vals[2]) * 0x100
                + int(ip_vals[3])
            )
        except (IndexError, ValueError):
            return 0x0
    # ipv6
    if ip_addr.find(":") != -1:
        try:
            acc = 0
            for ip_val in ip_addr.split(":"):
                acc *= 0x10000
                if ip_val:
                    acc += int(ip_val, 16)
            return acc
        except ValueError:
            return 0x0
    return 0x0


def task_import_url(task: Task) -> None:
    # find the last added Geoip ID
    try:
        (last_id,) = (
            db.session.query(Geoip.geoip_id).order_by(Geoip.geoip_id.desc()).first()
        )
    except TypeError:
        last_id = 0

    # download URL
    session = requests.Session()
    for geoip_url in app.config["GEOIP_URLS"]:
        task.percentage_current = 0
        task.status = f"Downloading {geoip_url}"
        db.session.commit()
        basename = os.path.basename(geoip_url)
        try:
            rv = session.get(geoip_url)
            payload: bytes = rv.content
        except requests.exceptions.RequestException as e:
            task.add_fail(f"Failed to download from {geoip_url}", str(e))
            return
        if rv.status_code != 200:
            task.add_fail(f"Failed to download from {rv.url}", rv.text)
            return

        # parse CSV zipped data
        task.status = "Decompressing"
        db.session.commit()
        if basename.endswith(".ZIP"):
            zfile = io.BytesIO(payload)
            with zipfile.ZipFile(zfile, "r") as zip_ref:
                payload = zip_ref.read(basename.replace(".ZIP", ""))

        # parse CSV gzipped data
        if basename.endswith(".gz"):
            try:
                payload = gzip.decompress(payload)
            except (OSError, UnicodeDecodeError) as e:
                task.add_fail("No GZipped object could be decoded", str(e))
                return

        # import the new data
        task.add_pass(f"Downloaded {basename} of {len(payload)} bytes")
        try:
            reader = csv.reader(StringIO(payload.decode(errors="ignore")))
        except ValueError as e:
            task.add_fail("No CSV object could be decoded", str(e))
            return
        countries: dict[str, int] = defaultdict(int)
        rows = list(reader)
        task.percentage_total = len(rows)
        task.status = "Adding new entries"
        db.session.commit()
        for row in rows:
            task.percentage_current += 1
            if task.percentage_current % 50 == 0:
                db.session.commit()
            try:
                if row[0].startswith("#"):
                    continue
                geo = Geoip(
                    addr_start=int(row[0]), addr_end=int(row[1]), country_code=row[2]
                )
                countries[row[2]] += 1
                if task.ended_ts:
                    break
                db.session.add(geo)
            except (IndexError, ValueError):
                pass

    # canceled
    if task.ended_ts:
        return

    # commit new IDs
    db.session.commit()
    task.add_pass("Added new rows")

    # remove old IDs
    task.status = "Deleting old entries"
    db.session.query(Geoip).filter(Geoip.geoip_id <= last_id).delete()
    db.session.commit()
    task.add_pass("Deleted old rows")

    # log
    task.add_pass(f"Imported GeoIP data: {countries!s}")
