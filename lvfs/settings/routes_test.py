#!/usr/bin/python3
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_settings(self, _app, client):
        # open the main page
        self.login()
        rv = client.get("/lvfs/settings/")
        assert b"General server settings" in rv.data, rv.data.decode()
        assert b"ClamAV" in rv.data, rv.data.decode()

        # dig into the Windows Update page
        rv = client.get("/lvfs/settings/wu-copy")
        assert b"Copy files generated" in rv.data, rv.data.decode()
        assert b'value="enabled" checked/>' in rv.data, rv.data.decode()

        # change both values to False
        rv = client.post(
            "/lvfs/settings/modify/wu-copy",
            data=dict(
                wu_copy_inf="disabled",
                wu_copy_cat="disabled",
            ),
            follow_redirects=True,
        )
        assert b"Copy files generated" in rv.data, rv.data.decode()
        assert b'value="enabled" />' in rv.data, rv.data.decode()

        # and back to True
        rv = client.post(
            "/lvfs/settings/modify/wu-copy",
            data=dict(
                wu_copy_inf="enabled",
                wu_copy_cat="enabled",
            ),
            follow_redirects=True,
        )
        assert b'value="enabled" checked/>' in rv.data, rv.data.decode()


if __name__ == "__main__":
    unittest.main()
