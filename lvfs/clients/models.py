#!/usr/bin/python3
#
# Copyright (C) 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

import datetime
from typing import Optional

from sqlalchemy import Column, Integer, Text, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from lvfs import db
from lvfs.hash import _addr_hash


class Client(db.Model):  # type: ignore
    __tablename__ = "clients"

    id = Column(Integer, primary_key=True)
    timestamp = Column(
        DateTime, nullable=False, default=datetime.datetime.utcnow, index=True
    )
    datestr = Column(Integer, default=0, index=True)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), nullable=False, index=True
    )
    user_agent = Column(Text, default=None)
    country_code = Column(Text, default=None)
    addr = Column(Text, default=None, index=True)

    fw = relationship("Firmware", foreign_keys=[firmware_id])

    @property
    def addr_hashed(self) -> Optional[str]:
        if not self.addr:
            return None
        return _addr_hash(self.addr)

    def __repr__(self) -> str:
        return f"Client({self.id})"


class ClientMetric(db.Model):  # type: ignore
    __tablename__ = "metrics"

    setting_id = Column(Integer, primary_key=True)
    key = Column(Text, nullable=False, index=True)
    value = Column(Integer, default=0)

    def __repr__(self) -> str:
        return f"ClientMetric({self.key}={self.value})"


class ClientAcl(db.Model):  # type: ignore
    __tablename__ = "client_acls"

    client_acl_id = Column(Integer, primary_key=True)
    addr = Column(Text, nullable=False, index=True, unique=True)
    message = Column(Text, nullable=False)
    comment = Column(Text, default=None)
    user_agent = Column(Text, default=None)
    resource = Column(Text, default=None)
    cnt = Column(Integer, default=0)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    mtime = Column(DateTime, default=None)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)

    user = relationship("User", foreign_keys=[user_id])

    def __repr__(self) -> str:
        return f"ClientAcl({self.client_acl_id})"
