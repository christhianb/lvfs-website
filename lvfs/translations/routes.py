#!/usr/bin/python3
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Any
from collections import defaultdict

from lxml import etree as ET

from flask import (
    Blueprint,
    request,
    url_for,
    redirect,
    flash,
    render_template,
    g,
    Response,
)
from flask_login import login_required
from sqlalchemy.orm.exc import NoResultFound

from lvfs import db

from lvfs.components.models import ComponentDescription, Component
from lvfs.vendors.models import VendorAffiliation
from lvfs.firmware.models import Firmware

bp_translations = Blueprint("translations", __name__, template_folder="templates")


@bp_translations.route("/")
@login_required
def route_list() -> Any:
    """
    Resources for translators.
    """
    # security check
    if not g.user.check_acl("@translate-export"):
        flash(
            "Permission denied: Unable to show event log for non-translator user",
            "danger",
        )
        return redirect(url_for("main.route_dashboard"))

    locales: dict[str, int] = defaultdict(int)
    for tx in (
        db.session.query(ComponentDescription)
        .filter(ComponentDescription.locale != None)
        .order_by(ComponentDescription.locale)
    ):
        locales[tx.locale] += 1

    return render_template(
        "translation-list.html",
        category="home",
        locales=locales,
    )


@bp_translations.route("/locale/<locale>")
@login_required
def route_locale(locale: str) -> Any:
    """
    Existing translations for a specific locale.
    """
    # security check
    if not g.user.check_acl("@translate-export"):
        flash(
            "Permission denied: Unable to show statistics for non-translator user",
            "danger",
        )
        return redirect(url_for("main.route_dashboard"))

    # add header
    n_xliff = ET.Element("xliff")
    n_xliff.set("xmlns", "urn:oasis:names:tc:xliff:document:2.0")
    n_xliff.set("version", "2.0")
    n_xliff.set("srcLang", "en-US")
    n_xliff.set("trgLang", locale.replace("_", "-"))

    # find all the affiliates vendors for this vendor
    vendor_ids = [g.user.vendor.vendor_id]
    for (vendor_id,) in (
        db.session.query(VendorAffiliation.vendor_id)
        .filter(VendorAffiliation.vendor_id_odm == g.user.vendor.vendor_id)
        .order_by(VendorAffiliation.vendor_id)
    ):
        vendor_ids.append(vendor_id)

    # find all the translations IDs that the user logged in can actually translate
    for tx in (
        db.session.query(ComponentDescription)
        .filter(ComponentDescription.locale == None)
        .join(Component)
        .join(Firmware)
        .filter(Firmware.vendor_id.in_(vendor_ids))
        .order_by(Component.component_id)
    ):
        # get already translated text, which may not exist
        try:
            targettext = tx.md.get_description_by_locale(locale).value
        except AttributeError:
            targettext = ""

        n_file = ET.SubElement(n_xliff, "file")
        n_file.set("id", str(tx.md.component_id))
        n_file.set("vendor", tx.md.fw.vendor.group_id)
        n_file.set("product", tx.md.fw.md_prio.appstream_id)
        n_unit = ET.SubElement(n_file, "unit")
        n_unit.set("id", "0")
        n_segment = ET.SubElement(n_unit, "segment")
        ET.SubElement(n_segment, "source").text = tx.value
        ET.SubElement(n_segment, "target").text = targettext

    # return raw XML
    xml = ET.tostring(
        n_xliff, encoding="utf-8", xml_declaration=True, pretty_print=True
    )
    return Response(response=xml, status=200, mimetype="application/xml")


@bp_translations.post("/upload")
@login_required
def route_upload():
    """Upload a XLIFF file to the LVFS service"""

    # security check
    if not g.user.check_acl("@translate-import"):
        flash(
            "Permission denied: Unable to upload for non-translator user",
            "danger",
        )
        return redirect(url_for("main.route_dashboard"))

    # create
    try:
        blob = request.files["file"].read()
    except KeyError as e:
        flash(f"Failed to upload: {e!s}", "warning")
        return redirect(url_for(".route_list"), code=302)
    try:
        parser = ET.XMLParser()
        ns = {"ns": "urn:oasis:names:tc:xliff:document:2.0"}
        n_xliff = ET.fromstring(blob, parser).xpath("/ns:xliff", namespaces=ns)[0]
    except (IndexError, UnicodeDecodeError) as e:
        flash(f"Invalid file: {e!s}", "warning")
        return redirect(url_for(".route_list"), code=302)
    except ET.XMLSyntaxError:
        item = parser.error_log[0]
        flash(f"Invalid XML on line {item.line}: {item.message}", "warning")
        return redirect(url_for(".route_list"), code=302)

    # check source locale
    try:
        src_locale = n_xliff.get("srcLang").replace("-", "_")
    except AttributeError as e:
        flash(f"Cannot parse source locale: {e!s}", "warning")
        return redirect(url_for(".route_list"), code=302)
    if src_locale != "en_US":
        flash("Source locale must be en_US", "warning")
        return redirect(url_for(".route_list"), code=302)

    # check target locale
    try:
        locale = n_xliff.get("trgLang").replace("-", "_")
    except AttributeError as e:
        flash(f"Cannot parse locale: {e!s}", "warning")
        return redirect(url_for(".route_list"), code=302)
    if locale == "en_US":
        flash(f"Source locale must not be {locale}", "warning")
        return redirect(url_for(".route_list"), code=302)

    # each component
    nr_changes = 0
    nr_additions = 0
    for n_file in n_xliff.xpath("ns:file", namespaces=ns):
        # find component
        try:
            component_id = int(n_file.get("id"))
        except TypeError as e:
            flash(f"Cannot parse component ID: {e!s}", "warning")
            continue
        try:
            md = (
                db.session.query(Component)
                .filter(Component.component_id == component_id)
                .one()
            )
        except NoResultFound:
            flash(f"Cannot find component ID {component_id}", "warning")
            continue

        # check permissions
        vendors_allowed = [md.fw.vendor]
        vendors_allowed.extend(md.fw.vendor.get_affiliates_with_action("@translate"))
        if g.user.vendor not in vendors_allowed:
            flash(f"No ACL for component ID {component_id}", "warning")
            continue

        # each resource
        for n_unit in n_file.xpath("ns:unit", namespaces=ns):
            # check the source is the same
            try:
                source = n_unit.xpath("ns:segment/ns:source", namespaces=ns)[0].text
            except IndexError as e:
                flash(
                    f"Cannot segment source for #{component_id}: {e!s}",
                    "warning",
                )
                continue
            tx = md.get_description_by_locale()
            if tx.value != source:
                flash(
                    f"Cannot change translation source for #{component_id}",
                    "warning",
                )
                continue

            # does this already exist
            try:
                target = n_unit.xpath("ns:segment/ns:target", namespaces=ns)[0].text
            except IndexError as e:
                flash(
                    f"Cannot segment target for #{component_id}: {e!s}",
                    "warning",
                )
                continue
            if not target:
                continue
            tx = md.get_description_by_locale(locale)
            if tx:
                nr_changes += 1
                tx.value = target
                tx.user = g.user
            else:
                nr_additions += 1
                md.descriptions.append(
                    ComponentDescription(value=target, user=g.user, locale=locale)
                )

    # success
    if nr_additions and nr_changes:
        flash(
            f"Uploaded {nr_additions} new translations and modified {nr_changes} others",
            "info",
        )
    elif nr_additions:
        flash(f"Uploaded {nr_additions} new translations", "info")
    elif nr_changes:
        flash(f"Modified {nr_changes} translations", "info")
    else:
        flash("No translations altered", "info")
    return redirect(url_for(".route_list"))
