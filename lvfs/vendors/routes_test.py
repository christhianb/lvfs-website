#!/usr/bin/python3
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_vendor_prodcert(self, _app, client):
        # public prodcert list
        rv = client.get("/lvfs/vendors/prodcerts")
        assert b"CH12345" not in rv.data, rv.data.decode()

        # add new product
        self.login()
        rv = client.post(
            "/lvfs/vendors/1/prodcert/create",
            data=dict(name="CH12345"),
            follow_redirects=True,
        )
        assert b"Added product certificatation" in rv.data, rv.data.decode()

        # check add the same
        rv = client.post(
            "/lvfs/vendors/1/prodcert/create",
            data=dict(name="CH12345"),
            follow_redirects=True,
        )
        assert b"Failed to add product certificatation" in rv.data, rv.data.decode()

        # add some details
        rv = client.post(
            "/lvfs/vendors/1/prodcert/1/modify",
            data={"name": "IC12345", "visible": "1"},
            follow_redirects=True,
        )
        assert b"Updated product certificatation" in rv.data, rv.data.decode()

        # show it in the per-vendor list
        rv = client.get("/lvfs/vendors/1/prodcert/list")
        assert b"CH12345" not in rv.data, rv.data.decode()
        assert b"IC12345" in rv.data, rv.data.decode()

        # make sure it's visible as non-logged in
        self.logout()
        rv = client.get("/lvfs/vendors/prodcerts")
        assert b"IC12345" in rv.data, rv.data.decode()

        # delete it
        self.login()
        rv = client.post(
            "/lvfs/vendors/1/prodcert/1/delete",
            follow_redirects=True,
        )
        assert b"Deleted product certificatation" in rv.data, rv.data.decode()
        self.logout()
        rv = client.get("/lvfs/vendors/prodcerts")
        assert b"IC12345" not in rv.data, rv.data.decode()

    def test_vendorlist(self, _app, client):
        # check users can't modify the list
        rv = client.get("/lvfs/vendors/admin")
        assert b"Create a new vendor" not in rv.data, rv.data.decode()

        # check admin can
        self.login()
        rv = client.get("/lvfs/vendors/admin")
        assert b"Create a new vendor" in rv.data, rv.data.decode()

        # create new vendor
        rv = client.post(
            "/lvfs/vendors/create",
            data=dict(group_id="testvendor"),
            follow_redirects=True,
        )
        assert b"Added vendor" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/vendors/admin")
        assert b"testvendor" in rv.data, rv.data.decode()

        # create duplicate
        rv = client.post(
            "/lvfs/vendors/create",
            data=dict(group_id="testvendor"),
            follow_redirects=True,
        )
        assert b"Group ID already exists" in rv.data, rv.data.decode()

        # show the details page
        rv = client.get("/lvfs/vendors/2/details")
        assert b"testvendor" in rv.data, rv.data.decode()

        # create a restriction
        rv = client.post(
            "/lvfs/vendors/2/restriction/create",
            data=dict(value="USB:0x1234"),
            follow_redirects=True,
        )
        assert b"Added restriction" in rv.data, rv.data.decode()

        # show the restrictions page
        rv = client.get("/lvfs/vendors/2/restrictions")
        assert b"USB:0x1234" in rv.data, rv.data.decode()

        # delete a restriction
        rv = client.post("/lvfs/vendors/2/restriction/1/delete", follow_redirects=True)
        assert b"Deleted restriction" in rv.data, rv.data.decode()
        assert b"USB:0x1234" not in rv.data, rv.data.decode()

        # create a namespace
        rv = client.post(
            "/lvfs/vendors/2/namespace/create",
            data=dict(value="com.dell"),
            follow_redirects=True,
        )
        assert b"Added namespace" in rv.data, rv.data.decode()

        # create a namespace
        rv = client.post(
            "/lvfs/vendors/2/namespace/create",
            data=dict(value="lenovo"),
            follow_redirects=True,
        )
        assert b"Failed to add namespace" in rv.data, rv.data.decode()

        # show the namespaces page
        rv = client.get("/lvfs/vendors/2/namespaces")
        assert b"com.dell" in rv.data, rv.data.decode()

        # delete a namespace
        rv = client.post("/lvfs/vendors/2/namespace/1/delete", follow_redirects=True)
        assert b"Deleted namespace" in rv.data, rv.data.decode()
        assert b"com.dell" not in rv.data, rv.data.decode()

        # create a tag
        rv = client.post(
            "/lvfs/vendors/2/tag/create", data=dict(name="SWID"), follow_redirects=True
        )
        assert b"Added tag" in rv.data, rv.data.decode()

        # create a tag without enough data
        rv = client.post(
            "/lvfs/vendors/2/tag/create", data=dict(dave="SWID"), follow_redirects=True
        )
        assert b"Failed to add tag" in rv.data, rv.data.decode()

        # modify a tag
        rv = client.post(
            "/lvfs/vendors/2/tag/1/modify",
            data=dict(name="SWID", example="ABCDEF", enforce="1"),
            follow_redirects=True,
        )
        assert b"Modified tag" in rv.data, rv.data.decode()

        # show the tags page
        rv = client.get("/lvfs/vendors/2/tags")
        assert b"SWID" in rv.data, rv.data.decode()
        assert b"ABCDEF" in rv.data, rv.data.decode()

        # show a firmware example
        self.upload(vendor_id=2)
        rv = client.get("/lvfs/components/1")
        assert b"SWID" in rv.data, rv.data.decode()
        assert b"ABCDEF" in rv.data, rv.data.decode()

        # enforce, so add as problem if missing
        rv = client.get("/lvfs/firmware/1/problems")
        assert b" The component requires a SWID" in rv.data, rv.data.decode()

        # add tag to firmware
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                release_tag="N1235",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()

        # verify the new problems
        rv = client.get("/lvfs/firmware/1/problems")
        assert b" The component requires a SWID" not in rv.data, rv.data.decode()

        # delete a tag
        rv = client.post("/lvfs/vendors/2/tag/1/delete", follow_redirects=True)
        assert b"Deleted tag" in rv.data, rv.data.decode()
        assert b"ABCDEF" not in rv.data, rv.data.decode()

        # create a branch
        rv = client.post(
            "/lvfs/vendors/2/branch/create",
            data=dict(value="dave"),
            follow_redirects=True,
        )
        assert b"Added branch" in rv.data, rv.data.decode()

        # create a branch
        rv = client.post(
            "/lvfs/vendors/2/branch/create",
            data=dict(value="dave"),
            follow_redirects=True,
        )
        assert b"Failed to add branch" in rv.data, rv.data.decode()

        # show the branches page
        rv = client.get("/lvfs/vendors/2/branches")
        assert b"dave" in rv.data, rv.data.decode()

        # delete a branch
        rv = client.post("/lvfs/vendors/2/branch/1/delete", follow_redirects=True)
        assert b"Deleted branch" in rv.data, rv.data.decode()
        assert b"dave" not in rv.data, rv.data.decode()

        # change some properties
        rv = client.post(
            "/lvfs/vendors/2/modify_by_admin",
            data=dict(
                display_name="VendorName",
                plugins="dfu 1.2.3",
                description="Everything supported",
                visible=True,
                keywords="keyword",
                comments="Emailed Dave on 2018-01-14 to follow up.",
            ),
            follow_redirects=True,
        )
        assert b"Updated vendor" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/vendors/admin")
        assert b"testvendor" in rv.data, rv.data.decode()

        # delete
        rv = client.post("/lvfs/vendors/2/delete", follow_redirects=True)
        assert b"Removed vendor" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/vendors/admin")
        assert b"testvendor" not in rv.data, rv.data.decode()

    def test_vendor_assets(self, _app, client):
        self.login()

        # upload asset
        with open(__file__, "rb") as fd:
            rv = client.post(
                "/lvfs/vendors/1/asset/upload",
                data={"file": (fd, os.path.basename(__file__))},
                follow_redirects=True,
            )
        assert b"XXXX" not in rv.data, rv.data.decode()

        # delete it
        rv = client.post("/lvfs/vendors/1/asset/1/delete", follow_redirects=True)
        assert b"XXXX" not in rv.data, rv.data.decode()

    def test_affiliation_change_as_admin(self, _app, client):
        # add oem and odm
        self.login()
        self.add_vendor("oem")  # 2
        self.add_user("alice@oem.com", "oem")
        rv = client.post(
            "/lvfs/vendors/2/modify_by_admin", data={}, follow_redirects=True
        )
        assert b"Updated vendor" in rv.data, rv.data.decode()
        self.add_vendor("odm")  # 3
        self.add_user("bob@odm.com", "odm")
        rv = client.post(
            "/lvfs/vendors/3/modify_by_admin", data={}, follow_redirects=True
        )
        assert b"Updated vendor" in rv.data, rv.data.decode()
        self.add_namespace(vendor_id=2)
        self.add_namespace(vendor_id=3)
        self.logout()

        # bob uploads to the ODM vendor
        self.login("bob@odm.com")
        self.upload(target="embargo")
        self.logout()

        # change the ownership to 'oem' as admin (no affiliation required)
        self.login()
        rv = client.get("/lvfs/firmware/1/affiliation")
        assert b'option value="3" selected' in rv.data, rv.data.decode()
        rv = client.post(
            "/lvfs/firmware/1/affiliation/change",
            data=dict(
                vendor_id="2",
            ),
            follow_redirects=True,
        )
        assert b"Changed firmware vendor" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/firmware/1/affiliation")
        assert b'option value="2" selected' in rv.data, rv.data.decode()
        rv = client.get("/lvfs/firmware/1")
        assert b">embargo-oem<" in rv.data, rv.data.decode()
        assert b">embargo-odm<" not in rv.data, rv.data.decode()

    def test_affiliation_change_as_qa(self, _app, client):
        # add oem and odm
        self.login()
        self.add_vendor("odm")  # 2
        self.add_vendor("oem")  # 3
        self.add_affiliation(3, 2)
        self.add_namespace(vendor_id=2)
        self.add_namespace(vendor_id=3)

        # add odm uploader and QA user
        self.add_user("alice@odm.com", "odm")
        self.add_user("bob@odm.com", "odm", is_qa=True)
        self.logout()

        # upload as alice
        self.login("alice@odm.com")
        self.upload(target="embargo", vendor_id=3)
        self.logout()

        # move to oem as bob
        self.login("bob@odm.com")
        rv = client.post(
            "/lvfs/firmware/1/affiliation/change",
            data=dict(
                vendor_id="2",
            ),
            follow_redirects=True,
        )
        assert b"Changed firmware vendor" in rv.data, rv.data.decode()

    def test_affiliations(self, _app, client):
        self.login()
        self.add_vendor("oem")  # 2
        self.add_user("alice@oem.com", "oem")
        self.add_vendor("odm")  # 3
        self.add_user("bob@odm.com", "odm", is_qa=True)
        self.add_vendor("another-unrelated-oem")  # 4

        rv = client.post(
            "/lvfs/vendors/2/modify_by_admin",
            data=dict(
                display_name="AliceOEM",
            ),
            follow_redirects=True,
        )
        assert b"Updated vendor" in rv.data, rv.data.decode()

        rv = client.post(
            "/lvfs/vendors/3/modify_by_admin",
            data=dict(
                display_name="BobOEM",
            ),
            follow_redirects=True,
        )
        assert b"Updated vendor" in rv.data, rv.data.decode()

        # no affiliations
        rv = client.get("/lvfs/vendors/2/affiliations")
        assert b"No affiliations exist" in rv.data, rv.data.decode()

        # add affiliation (as admin)
        self.add_affiliation(2, 3)
        rv = client.get("/lvfs/vendors/2/affiliations")
        assert b'<h2 class="card-title">\n      BobOEM' in rv.data, rv.data.decode()

        # add duplicate (as admin)
        rv = client.post(
            "/lvfs/vendors/2/affiliation/create",
            data=dict(
                vendor_id_odm="3",
            ),
            follow_redirects=True,
        )
        assert b"Already a affiliation with that ODM" in rv.data, rv.data.decode()

        # add namespace
        self.add_namespace(vendor_id=2, value="com.hughski")

        # add and remove actions
        rv = client.post(
            "/lvfs/vendors/2/affiliation/1/action/create/DAVE", follow_redirects=True
        )
        assert b"Failed to add action: Expected" in rv.data, rv.data.decode()
        rv = client.post(
            "/lvfs/vendors/2/affiliation/1/action/create/@test", follow_redirects=True
        )
        assert b"Added action" in rv.data, rv.data.decode()
        rv = client.post(
            "/lvfs/vendors/2/affiliation/1/action/remove/@notgoingtoexist",
            follow_redirects=True,
        )
        assert b"Failed to remove action: Not present" in rv.data, rv.data.decode()
        rv = client.post(
            "/lvfs/vendors/2/affiliation/1/action/remove/@test", follow_redirects=True
        )
        assert b"Removed action" in rv.data, rv.data.decode()

        self.logout()

        # test uploading as the ODM to a vendor_id that does not exist
        self.login("bob@odm.com")
        rv = self._upload(vendor_id=999)
        assert b"Specified vendor ID not found" in rv.data, rv.data.decode()

        # test uploading as the ODM to a vendor_id without an affiliation
        rv = self._upload(vendor_id=4)
        assert b"Failed to upload file for vendor" in rv.data, rv.data.decode()

        # test uploading to a OEM account we have an affiliation with
        self.upload(vendor_id=2)

        # check bob can see the firmware he uploaded
        rv = client.get("/lvfs/firmware/1")
        assert b"ColorHug2" in rv.data, rv.data.decode()

        # check bob can change the update description and severity
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                urgency="critical",
                description="Not enough cats!",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()
        self.run_task_worker()

        # check bob can move the firmware to the embargo remote for the *OEM*
        rv = client.get("/lvfs/firmware/1/target")
        assert b"/promote/embargo" in rv.data, rv.data.decode()
        rv = client.post("/lvfs/firmware/1/promote/embargo", follow_redirects=True)
        assert b"Moving firmware" in rv.data, rv.data.decode()
        self.run_task_worker()
        rv = client.get("/lvfs/firmware/1/target", follow_redirects=True)
        assert b">embargo-oem<" in rv.data, rv.data.decode()
        assert b">embargo-odm<" not in rv.data, rv.data.decode()

        # check bob can't move the firmware to stable
        rv = client.post("/lvfs/firmware/1/promote/stable", follow_redirects=True)
        assert b"Permission denied" in rv.data, rv.data.decode()
        self.logout()

        # remove affiliation as admin
        self.login()
        rv = client.post("/lvfs/vendors/2/affiliation/1/delete", follow_redirects=True)
        assert b"Deleted affiliation" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/vendors/2/affiliations")
        assert b"No affiliations exist" in rv.data, rv.data.decode()

    def test_relationships(self, _app, client):
        self.login()
        self.add_vendor("oem")  # 2
        self.add_user("alice@oem.com", "oem")
        self.add_vendor("odm")  # 3
        self.add_user("bob@odm.com", "odm", is_qa=True)
        self.add_vendor("another-unrelated-oem")  # 4

        rv = client.post(
            "/lvfs/vendors/2/modify_by_admin",
            data=dict(
                display_name="AliceOEM",
            ),
            follow_redirects=True,
        )
        assert b"Updated vendor" in rv.data, rv.data.decode()

        rv = client.post(
            "/lvfs/vendors/3/modify_by_admin",
            data=dict(
                display_name="BobOEM",
            ),
            follow_redirects=True,
        )
        assert b"Updated vendor" in rv.data, rv.data.decode()

        # no relationships
        rv = client.get("/lvfs/vendors/2/relationships")
        assert b"No relationships exist" in rv.data, rv.data.decode()

        # add relationship (as admin)
        self.add_relationship(2, 3)
        rv = client.get("/lvfs/vendors/2/relationships")
        assert b'<h2 class="card-title">\n      BobOEM' in rv.data, rv.data.decode()

        # add duplicate (as admin)
        rv = client.post(
            "/lvfs/vendors/2/relationship/create",
            data=dict(
                vendor_id_odm="3",
            ),
            follow_redirects=True,
        )
        assert b"Already a relationship with that ODM" in rv.data, rv.data.decode()

        # remove relationship as admin
        self.login()
        rv = client.post("/lvfs/vendors/2/relationship/1/delete", follow_redirects=True)
        assert b"Deleted relationship" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/vendors/2/relationships")
        assert b"No relationships exist" in rv.data, rv.data.decode()


if __name__ == "__main__":
    unittest.main(module="routes_test")
