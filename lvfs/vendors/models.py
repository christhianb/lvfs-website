#!/usr/bin/python3
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

import os
import datetime
import enum

from typing import Optional, Any

from flask import g
from flask import current_app as app

from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    Text,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship

from lvfs import db

from lvfs.users.models import User


class VendorAffiliationAction(db.Model):  # type: ignore
    __tablename__ = "affiliation_actions"

    affiliation_action_id = Column(Integer, primary_key=True)
    affiliation_id = Column(
        Integer, ForeignKey("affiliations.affiliation_id"), nullable=False, index=True
    )
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    action = Column(Text, default=None)

    user = relationship("User", foreign_keys=[user_id])
    affiliation = relationship("VendorAffiliation", foreign_keys=[affiliation_id])

    def __repr__(self) -> str:
        return f"VendorAffiliationAction({self.action})"


class VendorAffiliation(db.Model):  # type: ignore
    __tablename__ = "affiliations"

    affiliation_id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    vendor_id_odm = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )

    vendor = relationship(
        "Vendor", foreign_keys=[vendor_id], back_populates="affiliations"
    )
    vendor_odm = relationship("Vendor", foreign_keys=[vendor_id_odm])
    actions = relationship(
        "VendorAffiliationAction",
        cascade="all,delete,delete-orphan",
        back_populates="affiliation",
    )

    def get_action(self, action: str) -> Optional[VendorAffiliationAction]:
        for act in self.actions:
            if action == act.action:
                return act
        return None

    def __repr__(self) -> str:
        return f"VendorAffiliation({self.affiliation_id})"


class VendorRelationship(db.Model):  # type: ignore
    __tablename__ = "vendor_relationships"

    vendor_relationships_id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    vendor_id_odm = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)

    user = relationship("User", foreign_keys=[user_id])
    vendor = relationship(
        "Vendor", foreign_keys=[vendor_id], back_populates="relationships"
    )
    vendor_odm = relationship("Vendor", foreign_keys=[vendor_id_odm])

    def __repr__(self) -> str:
        return f"VendorRelationship({self.vendor_relationships_id})"


class VendorRestriction(db.Model):  # type: ignore
    __tablename__ = "restrictions"

    restriction_id = Column(Integer, primary_key=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    value = Column(Text, nullable=False, index=True)

    vendor = relationship("Vendor", back_populates="restrictions")

    def __repr__(self) -> str:
        return f"VendorRestriction({self.restriction_id})"


class VendorBranch(db.Model):  # type: ignore
    __tablename__ = "vendor_branches"

    branch_id = Column(Integer, primary_key=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    value = Column(Text, nullable=False)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)

    vendor = relationship("Vendor", back_populates="branches")
    user = relationship("User", foreign_keys=[user_id])

    __table_args__ = (
        UniqueConstraint(
            "vendor_id", "value", name="uq_vendor_branches_vendor_id_value"
        ),
    )

    def __repr__(self) -> str:
        return f"VendorBranch({self.value})"


class VendorNamespace(db.Model):  # type: ignore
    __tablename__ = "namespaces"

    namespace_id = Column(Integer, primary_key=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    value = Column(Text, nullable=False)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)

    vendor = relationship("Vendor", back_populates="namespaces")
    user = relationship("User", foreign_keys=[user_id])

    @property
    def is_valid(self) -> bool:
        if self.value.endswith("."):
            return False
        if self.value.find(".") == -1:
            return False
        return True

    def __repr__(self) -> str:
        return f"VendorNamespace({self.value})"


class VendorTag(db.Model):  # type: ignore
    __tablename__ = "vendor_tags"

    vendor_tag_id = Column(Integer, primary_key=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    name = Column(Text, default=None)  # e.g. SWB
    example = Column(Text, default=None)  # e.g. N1CET75W
    details_url = Column(Text, default=None)  # e.g. https://foo/$RELEASE_TAG$/baz
    enforce = Column(Boolean, default=False)
    category_id = Column(Integer, ForeignKey("categories.category_id"), nullable=True)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)

    vendor = relationship("Vendor", back_populates="tags")
    user = relationship("User", foreign_keys=[user_id])
    category = relationship("Category")

    def __repr__(self) -> str:
        return f"VendorTag({self.vendor_tag_id})"


class VendorAsset(db.Model):  # type: ignore
    __tablename__ = "vendor_assets"

    vendor_asset_id = Column(Integer, primary_key=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    filename = Column(Text, nullable=False)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)

    vendor = relationship("Vendor", back_populates="assets")
    user = relationship("User", foreign_keys=[user_id])

    @property
    def absolute_path(self) -> str:
        return os.path.join(app.config["DOWNLOAD_DIR"], self.filename)

    @property
    def url(self) -> str:
        return os.path.join("/downloads", self.filename)

    def __repr__(self) -> str:
        return f"VendorAsset({self.vendor_asset_id})"


class VendorProdcertSignatureKind(enum.Enum):
    UNKNOWN = 0
    UNSIGNED = 1
    SIGNED = 2
    COMPLICATED = 3


class VendorProdcert(db.Model):  # type: ignore
    __tablename__ = "vendor_prodcert"

    vendor_prodcert_id = Column(Integer, primary_key=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    visible = Column(Boolean, default=False)
    name = Column(Text, nullable=False)
    summary = Column(Text, default=None)
    url = Column(Text, default=None)
    signature_kind = Column(Integer, default=0)
    signature_details = Column(Text, default=None)

    vendor = relationship("Vendor", back_populates="prodcerts")
    user = relationship("User", foreign_keys=[user_id])

    __table_args__ = (
        UniqueConstraint("vendor_id", "name", name="uq_vendor_prodcert_vendor_id_name"),
    )

    @property
    def icon(self) -> Optional[str]:
        if self.signature_kind == VendorProdcertSignatureKind.SIGNED.value:
            return "prodcert-signed"
        if self.signature_kind == VendorProdcertSignatureKind.UNSIGNED.value:
            return "prodcert-unsigned"
        if self.signature_kind == VendorProdcertSignatureKind.COMPLICATED.value:
            return "prodcert-complicated"
        return None

    def __repr__(self) -> str:
        return f"VendorProdcert({self.vendor_prodcert_id})"


class Vendor(db.Model):  # type: ignore
    __tablename__ = "vendors"

    vendor_id = Column(Integer, primary_key=True)
    group_id = Column(String(80), nullable=False, index=True, unique=True)
    display_name = Column(Text, default=None)
    legal_name = Column(Text, default=None)
    internal_team = Column(Text, default=None)
    quote_text = Column(Text, default=None)
    quote_author = Column(Text, default=None)
    consulting_text = Column(Text, default=None)
    consulting_link = Column(Text, default=None)
    relationship_text = Column(Text, default=None)
    visible = Column(Boolean, default=False)
    visible_for_search = Column(Boolean, default=False)
    visible_on_landing = Column(Boolean, default=False)
    is_embargo_default = Column(Boolean, default=False)
    is_community = Column(Boolean, default=False)
    comments = Column(Text, default=None)
    icon = Column(Text, default=None)
    keywords = Column(Text, default=None)
    oauth_unknown_user = Column(Text, default=None)
    oauth_domain_glob = Column(Text, default=None)
    remote_id = Column(Integer, ForeignKey("remotes.remote_id"), nullable=False)
    username_glob = Column(Text, default=None)
    verfmt_id = Column(Integer, ForeignKey("verfmts.verfmt_id"))
    fws_stable = Column(Integer, default=0)
    fws_stable_recent = Column(Integer, default=0)
    url = Column(Text, default=None)
    psirt_url = Column(Text, default=None)
    missing_url = Column(Text, default=None)
    banned_country_codes = Column(Text, default=None)  # ISO 3166, delimiter ','
    do_not_track = Column(Boolean, default=False)
    auth_required_for_metadata = Column(Boolean, default=False)
    _unused_default_inf_parsing = Column("default_inf_parsing", Boolean, default=False)
    is_odm = Column(Boolean, default=False)
    subgroups = Column(Text, default=None)  # delimiter ','
    _unused_relaxed_upload_rules = Column(
        "relaxed_upload_rules", Boolean, default=False
    )
    allow_component_tags = Column(Boolean, default=False)
    ignore_require_report = Column(Boolean, default=False)

    users = relationship(
        "User", back_populates="vendor", cascade="all,delete,delete-orphan"
    )
    prodcerts = relationship(
        "VendorProdcert",
        back_populates="vendor",
        cascade="all,delete,delete-orphan",
        order_by="asc(VendorProdcert.name)",
    )
    groups = relationship(
        "UserGroup", back_populates="vendor", cascade="all,delete,delete-orphan"
    )
    tags = relationship(
        "VendorTag", back_populates="vendor", cascade="all,delete,delete-orphan"
    )
    assets = relationship(
        "VendorAsset", back_populates="vendor", cascade="all,delete,delete-orphan"
    )
    restrictions = relationship(
        "VendorRestriction", back_populates="vendor", cascade="all,delete,delete-orphan"
    )
    namespaces = relationship(
        "VendorNamespace", back_populates="vendor", cascade="all,delete,delete-orphan"
    )
    branches = relationship(
        "VendorBranch", back_populates="vendor", cascade="all,delete,delete-orphan"
    )
    affiliations = relationship(
        "VendorAffiliation",
        foreign_keys=[VendorAffiliation.vendor_id],
        back_populates="vendor",
        cascade="all,delete,delete-orphan",
    )
    affiliations_for = relationship(
        "VendorAffiliation",
        foreign_keys=[VendorAffiliation.vendor_id_odm],
        back_populates="vendor_odm",
    )
    relationships = relationship(
        "VendorRelationship",
        foreign_keys=[VendorRelationship.vendor_id],
        back_populates="vendor",
        cascade="all,delete,delete-orphan",
    )
    relationships_for = relationship(
        "VendorRelationship",
        foreign_keys=[VendorRelationship.vendor_id_odm],
        back_populates="vendor_odm",
    )
    fws = relationship(
        "Firmware",
        foreign_keys="[Firmware.vendor_id]",
        cascade="all,delete,delete-orphan",
        back_populates="vendor",
    )
    mdrefs = relationship(
        "ComponentRef",
        foreign_keys="[ComponentRef.vendor_id_partner]",
        cascade="all,delete,delete-orphan",
        back_populates="vendor_partner",
    )
    events = relationship(
        "Event",
        order_by="desc(Event.timestamp)",
        lazy="dynamic",
        cascade="all,delete,delete-orphan",
    )

    verfmt = relationship("Verfmt", foreign_keys=[verfmt_id])
    remote = relationship(
        "Remote",
        foreign_keys=[remote_id],
        single_parent=True,
        cascade="all,delete,delete-orphan",
    )

    @property
    def protocols(self) -> list[Any]:
        from lvfs.firmware.models import Firmware
        from lvfs.components.models import Component
        from lvfs.metadata.models import Remote
        from lvfs.protocols.models import Protocol

        return (
            db.session.query(Protocol)
            .join(Component)
            .join(Firmware)
            .filter(Firmware.vendor_id == self.vendor_id)
            .join(Remote)
            .filter(Remote.name == "stable")
            .order_by(Protocol.name.asc())
            .all()
        )

    @property
    def regid(self) -> Optional[str]:
        regid = self.url
        if not regid:
            return None
        for prefix in ["https://", "http://", "www."]:
            if regid.startswith(prefix):
                regid = regid[len(prefix) :]
        if regid.endswith("/"):
            regid = regid[:-1]
        return regid

    @property
    def is_account_holder(self) -> bool:
        return self.users

    @property
    def subgroups_fallback(self) -> str:
        return self.subgroups or "TPE,ME"

    @property
    def should_anonymize(self) -> bool:
        if self.group_id == "hughski":  # this is my hobby; I have no secrets
            return False
        return True

    @property
    def is_unrestricted(self) -> bool:
        for res in self.restrictions:
            if res.value == "*":
                return True
        return False

    @property
    def display_name_with_team(self) -> str:
        if self.internal_team:
            return f"{self.display_name} ({self.internal_team})"
        if self.is_community:
            return "Community Supported"
        return self.display_name

    @property
    def ctime(self):
        val = None
        for user in self.users:
            if not user.ctime:
                continue
            if not val or user.ctime < val:
                val = user.ctime
        return val

    @property
    def mtime(self):
        val = None
        for user in self.users:
            if not user.mtime:
                continue
            if not val or user.mtime > val:
                val = user.mtime
        return val

    @property
    def atime(self):
        val = None
        for user in self.users:
            if not user.atime:
                continue
            if not val or user.atime > val:
                val = user.atime
        return val

    def is_affiliate_for(self, vendor_id: int) -> bool:
        for rel in self.affiliations_for:
            if rel.vendor_id == vendor_id:
                return True
        return False

    def is_affiliate(self, vendor_id_odm: int) -> bool:
        for rel in self.affiliations:
            if rel.vendor_id_odm == vendor_id_odm:
                return True
        return False

    def is_relationship_for(self, vendor_id: int) -> bool:
        for rel in self.relationships_for:
            if rel.vendor_id == vendor_id:
                return True
        return False

    def get_affiliates_with_action(self, action: str) -> list["Vendor"]:
        results: list["Vendor"] = []
        for rel in self.affiliations:
            if rel.get_action(action):
                results.append(rel.vendor_odm)
        return results

    def check_acl(self, action: str, user: Optional[User] = None) -> bool:
        # fall back
        if not user:
            user = g.user
        if not user:
            return False
        if user.check_acl("@admin"):
            return True

        # depends on the action requested
        if action == "@upload":
            # all members of a group can upload to that group
            if user.vendor_id == self.vendor_id:
                return True
            # allow vendor affiliates too
            if self.is_affiliate(user.vendor_id):
                return True
            return False
        if action == "@view-metadata":
            # all members of a group can generate the metadata file
            if user.vendor_id == self.vendor_id:
                return True
            return False
        if action == "@manage-users":
            if user.vendor_id != self.vendor_id:
                return False
            # manager user can modify any users in his group
            if user.check_acl("@vendor-manager"):
                return True
            return False
        if action == "@modify-oauth":
            return False
        if action == "@view-affiliations":
            if user.vendor_id != self.vendor_id:
                return False
            return user.check_acl("@vendor-manager")
        if action == "@view-relationships":
            if user.vendor_id != self.vendor_id:
                return False
            return user.check_acl("@vendor-manager")
        if action == "@view-restrictions":
            if user.vendor_id != self.vendor_id:
                return False
            return user.check_acl("@vendor-manager")
        if action == "@modify-affiliations":
            return False
        if action == "@modify-affiliation-actions":
            if user.vendor_id != self.vendor_id:
                return False
            return user.check_acl("@vendor-manager")
        if action == "@modify-relationships":
            return False
        if action == "@view-exports":
            if user.vendor_id != self.vendor_id:
                return False
            return user.check_acl("@qa") or user.check_acl("@vendor-manager")
        if action == "@modify-exports":
            return user.check_acl("@vendor-manager")
        raise NotImplementedError(f"unknown security check action: {self}:{action}")

    def __hash__(self) -> int:
        return int(self.vendor_id)

    def __lt__(self, other: Any) -> bool:
        return self.vendor_id < other.vendor_id

    def __eq__(self, other: Any) -> bool:
        return self.vendor_id == other.vendor_id

    def __repr__(self) -> str:
        return f"Vendor({self.group_id})"
