#!/usr/bin/python3
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-many-locals,too-many-statements,too-few-public-methods,protected-access

from typing import Any


def init_db(db: Any) -> None:
    # ensure all tables exist
    db.metadata.create_all(bind=db.engine)

    # ensure admin user exists
    from .vendors.models import Vendor
    from .verfmts.models import Verfmt
    from .protocols.models import Protocol
    from .categories.models import Category
    from .licenses.models import License
    from .users.models import User, UserAction
    from .metadata.models import Remote
    from .tasks.models import Task, TaskScheduler
    from .tasks.utils import task_create_schedulers
    from .hash import _otp_hash

    if not db.session.query(TaskScheduler).first():
        db.session.add(
            TaskScheduler(
                function="lvfs.tasks.utils.task_create_schedulers",
                interval=24 * 30,
                priority=-10,
            )
        )
        task = Task()
        task_create_schedulers(task)
    if not db.session.query(Remote).filter(Remote.name == "stable").first():
        db.session.add(Remote(name="stable", is_public=True))
        db.session.add(Remote(name="testing", is_public=True))
        db.session.add(Remote(name="private"))
        db.session.add(Remote(name="deleted"))
        db.session.commit()
    if not db.session.query(Verfmt).filter(Verfmt.value == "triplet").first():
        db.session.add(Verfmt(value="quad", name="Quad"))
        db.session.add(Verfmt(value="triplet", name="Triplet"))
        db.session.add(Verfmt(value="pair", name="Pair"))
        db.session.add(Verfmt(value="plain", name="Plain"))
        db.session.add(Verfmt(value="hex", name="Hex"))
        db.session.commit()
    if (
        not db.session.query(Protocol)
        .filter(Protocol.value == "com.hughski.colorhug")
        .first()
    ):
        db.session.add(
            Protocol(
                value="com.hughski.colorhug",
                name="ColorHug",
                is_public=True,
                allow_device_category=True,
            )
        )
        db.session.add(
            Protocol(
                value="org.usb.dfu",
                name="USB DFU",
                is_public=True,
                allow_device_category=True,
            )
        )
        db.session.add(
            Protocol(
                value="org.uefi.capsule",
                name="UEFI Capsule",
                is_public=True,
                allow_device_category=True,
                update_request_id="org.freedesktop.fwupd.request.do-not-power-off",
                update_message="Do not turn off your computer or remove the AC adapter "
                "while the update is in progress.",
            )
        )
        db.session.add(
            Protocol(
                value="org.dmtf.redfish",
                name="Redfish",
                is_public=True,
                allow_device_category=True,
            )
        )
        db.session.commit()
    if not db.session.query(Category).filter(Category.value == "X-System").first():
        db.session.add(Category(value="X-System", name="System Update"))
        db.session.add(Category(value="X-Device", name="Device Update"))
        db.session.add(
            Category(value="X-EmbeddedController", name="Embedded Controller Update")
        )
        db.session.add(
            Category(value="X-ManagementEngine", name="Management Engine Update")
        )
        db.session.add(Category(value="X-Controller", name="Controller Update"))
        db.session.add(
            Category(value="X-CorporateManagementEngine", name="Corporate ME Update")
        )
        db.session.add(
            Category(value="X-ConsumerManagementEngine", name="Consumer ME Update")
        )
        db.session.add(
            Category(
                value="X-ThunderboltController", name="Thunderbolt Controller Update"
            )
        )
        db.session.add(
            Category(
                value="X-PlatformSecurityProcessor",
                name="Platform Security Processor Update",
            )
        )
        db.session.add(Category(value="X-CpuMicrocode", name="CPU Microcode Update"))
        db.session.add(Category(value="X-Configuration", name="Configuration Update"))
        db.session.add(Category(value="X-Battery", name="Battery Update"))
        db.session.add(Category(value="X-Camera", name="Camera Update"))
        db.session.add(Category(value="X-TPM", name="TPM Update"))
        db.session.add(Category(value="X-Touchpad", name="Touchpad Update"))
        db.session.add(Category(value="X-Mouse", name="Mouse Update"))
        db.session.add(Category(value="X-Keyboard", name="Keyboard Update"))
        db.session.add(
            Category(value="X-StorageController", name="Storage Controller Update")
        )
        db.session.add(
            Category(value="X-NetworkInterface", name="Network Interface Update")
        )
        db.session.add(Category(value="X-VideoDisplay", name="Video Display Update"))
        db.session.add(
            Category(value="X-BaseboardManagementController", name="BMC Update")
        )
        db.session.add(Category(value="X-UsbReceiver", name="USB Receiver Update"))
        db.session.add(Category(value="X-Drive", name="Drive Update"))
        db.session.add(Category(value="X-FlashDrive", name="Flash Drive Update"))
        db.session.add(Category(value="X-SolidStateDrive", name="SSD Update"))
        db.session.commit()
    if not db.session.query(License).filter(License.value == "CC0-1.0").first():
        db.session.add(License(value="CC0-1.0", is_content=True))
        db.session.add(License(value="GPL-2.0+", requires_source=True))
        db.session.add(License(value="LicenseRef-proprietary"))
        db.session.commit()
    if (
        not db.session.query(User)
        .filter(User.username == "sign-test@fwupd.org")
        .first()
    ):
        remote = Remote(name="embargo-admin")
        remote.access_token = "admin"
        db.session.add(remote)
        db.session.commit()
        vendor = Vendor(group_id="admin")
        vendor.display_name = "Acme"
        vendor.legal_name = "Acme Corp."
        vendor.psirt_url = "https://acme.com/psirt"
        vendor.missing_url = "https://acme.com/missing"
        vendor.subgroups = "dummy"
        vendor.remote_id = remote.remote_id
        db.session.add(vendor)
        db.session.commit()
        u = User(
            username="sign-test@fwupd.org",
            auth_type="local",
            otp_secret=_otp_hash(),
            display_name="Admin User",
            vendor_id=vendor.vendor_id,
        )
        u.actions.append(UserAction(value="admin"))
        u.actions.append(UserAction(value="qa"))
        u.actions.append(UserAction(value="analyst"))
        u.actions.append(UserAction(value="partner"))
        u.actions.append(UserAction(value="notify-server-error"))
        u.password = "Pa$$w0rd"
        db.session.add(u)
        db.session.commit()
    if not db.session.query(User).filter(User.username == "anon@fwupd.org").first():
        db.session.add(
            User(username="anon@fwupd.org", display_name="Anonymous User", vendor_id=1)
        )
        db.session.commit()


def drop_db(db: Any) -> None:
    db.metadata.drop_all(bind=db.engine)
