#!/usr/bin/python3
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,protected-access

import datetime
import enum
from typing import Optional
from types import TracebackType

from flask import g

from sqlalchemy import Column, Integer, Text, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from lvfs import db

from lvfs.users.models import User


class TestResult(enum.IntEnum):
    UNKNOWN = 0
    PASS = 10
    WARN = 20
    FAIL = 30


class TestAttribute(db.Model):  # type: ignore
    __tablename__ = "test_attributes"

    test_attribute_id = Column(Integer, primary_key=True)
    test_id = Column(Integer, ForeignKey("tests.test_id"), nullable=False, index=True)
    title = Column(Text, nullable=False)
    message = Column(Text, default=None)
    _success = Column("success", Boolean, default=False)
    result = Column(Integer, default=TestResult.UNKNOWN)

    test = relationship("Test", back_populates="attributes")

    def __repr__(self) -> str:
        return f"TestAttribute({self.title},{self.result})"


class Test(db.Model):  # type: ignore
    __tablename__ = "tests"

    test_id = Column(Integer, primary_key=True)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), nullable=False, index=True
    )
    plugin_id = Column(Text, default=None, index=True)
    container_id = Column(Text, default=None)
    waivable = Column(Boolean, default=False)
    scheduled_ts = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    started_ts = Column(DateTime, default=None, index=True)
    ended_ts = Column(DateTime, default=None, index=True)
    waived_ts = Column(DateTime, default=None, index=True)
    waived_user_id = Column(Integer, ForeignKey("users.user_id"), nullable=True)
    max_age = Column(Integer, default=0)

    waived_user = relationship("User", foreign_keys=[waived_user_id])
    attributes = relationship(
        "TestAttribute",
        lazy="joined",
        back_populates="test",
        cascade="all,delete,delete-orphan",
    )

    fw = relationship("Firmware", back_populates="tests")

    def add_pass(self, title: str, message: Optional[str] = None) -> None:
        self.attributes.append(
            TestAttribute(title=title, message=message, result=TestResult.PASS)
        )

    def add_fail(self, title: str, message: Optional[str] = None) -> None:
        self.attributes.append(
            TestAttribute(title=title, message=message, result=TestResult.FAIL)
        )

    def add_warn(self, title: str, message: Optional[str] = None) -> None:
        self.attributes.append(
            TestAttribute(title=title, message=message, result=TestResult.WARN)
        )

    def waive(self) -> None:
        self.waived_ts = datetime.datetime.utcnow()
        self.waived_user_id = g.user.user_id

    def unwaive(self) -> None:
        self.waived_ts = None
        self.waived_user_id = None

    def retry(self) -> None:
        self.scheduled_ts = datetime.datetime.utcnow()
        self.started_ts = None
        self.ended_ts = None
        for attr in self.attributes:
            db.session.delete(attr)

    def check_acl(self, action: str, user: Optional[User] = None) -> bool:
        # fall back
        if not user:
            user = g.user
        if not user:
            return False
        if user.check_acl("@admin"):
            return True

        # depends on the action requested
        if action == "@retry":
            if user.check_acl("@qa") and self.fw._is_permitted_action(action, user):
                return True
            if self.fw._is_owner(user):
                return True
            return False
        if action == "@waive":
            if user.check_acl("@qa") and self.fw._is_permitted_action(action, user):
                return True
            return False
        raise NotImplementedError(f"unknown security check action: {self}:{action}")

    @property
    def timestamp(self) -> DateTime:
        if self.ended_ts:
            return self.ended_ts
        if self.started_ts:
            return self.started_ts
        return self.scheduled_ts

    @property
    def is_pending(self) -> bool:
        if not self.started_ts:
            return True
        return False

    @property
    def is_waived(self) -> bool:
        if self.waived_ts:
            return True
        return False

    @property
    def is_running(self) -> bool:
        if self.started_ts and not self.ended_ts:
            return True
        return False

    @property
    def color(self) -> str:
        if self.result == TestResult.PASS:
            return "success"
        if self.result == TestResult.WARN:
            return "warning"
        if self.is_running:
            return "info"
        if self.is_pending:
            return "info"
        if self.is_waived:
            return "warning"
        return "danger"

    @property
    def result(self) -> TestResult:
        result = TestResult.PASS
        for attr in self.attributes:
            if attr.result:
                result = max(result, attr.result)
        return result

    def __enter__(self) -> None:
        self.started_ts = datetime.datetime.utcnow()
        db.session.commit()

    def __exit__(
        self,
        exc_type: Optional[type[BaseException]],
        exc_value: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> None:
        self.ended_ts = datetime.datetime.utcnow()
        db.session.commit()

    def __repr__(self) -> str:
        return f"Test({self.plugin_id},{self.result})"
