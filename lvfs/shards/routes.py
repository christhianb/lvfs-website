#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from collections import defaultdict
from typing import Dict, List, Any
from io import StringIO
import json
import csv

from uuid import UUID

from flask import (
    Blueprint,
    flash,
    make_response,
    redirect,
    render_template,
    request,
    url_for,
    g,
)
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db, cache

from lvfs.components.models import (
    Component,
    ComponentShard,
    ComponentShardClaim,
    ComponentShardInfo,
)
from lvfs.claims.models import Claim
from lvfs.util import admin_login_required, _validate_guid


bp_shards = Blueprint("shards", __name__, template_folder="templates")


@bp_shards.route("/")
@login_required
def route_list() -> Any:
    # security check
    if not g.user.check_acl("@partner"):
        return redirect(url_for("main.route_dashboard"), 302)

    # only show shards with the correct group_id
    shards = (
        db.session.query(ComponentShardInfo)
        .order_by(ComponentShardInfo.cnt.desc())
        .all()
    )
    return render_template("shard-list.html", category="firmware", shards=shards)


@bp_shards.post("/create")
@login_required
def route_create() -> Any:
    # security check
    if not g.user.check_acl("@partner"):
        return redirect(url_for("main.route_dashboard"), 302)

    # ensure has enough data
    if "guid" not in request.form:
        flash("No form data found", "warning")
        return redirect(url_for("shards.route_list"))
    guid = request.form["guid"].lower()
    try:
        _ = UUID(guid)
    except ValueError:
        flash("Failed to add shard: Not a GUID", "warning")
        return redirect(url_for("shards.route_list"))

    # add ComponentShardInfo
    try:
        info = ComponentShardInfo(guid=guid)
        db.session.add(info)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add shard: Already exists", "info")
        return redirect(url_for("shards.route_list"))
    flash("Added shard", "info")
    return redirect(
        url_for(
            "shards.route_show", component_shard_info_id=info.component_shard_info_id
        )
    )


@bp_shards.post("/<int:component_shard_info_id>/modify")
@login_required
def route_modify(component_shard_info_id: int) -> Any:
    # security check
    if not g.user.check_acl("@partner"):
        return redirect(url_for("main.route_dashboard"), 302)

    # find shard
    try:
        shard = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .with_for_update(of=ComponentShardInfo)
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # modify shard
    keys: List[str] = []
    if g.user.check_acl("@partner"):
        keys.append("name")
        keys.append("description")
    if g.user.check_acl("@admin"):
        keys.append("claim_id")
    for key in keys:
        if key in request.form:
            setattr(shard, key, request.form[key] or None)
    db.session.commit()

    # success
    flash("Modified shard", "info")
    return route_show(component_shard_info_id)


@bp_shards.post("/<int:component_shard_info_id>/delete")
@login_required
def route_delete(component_shard_info_id: int) -> Any:
    # security check
    if not g.user.check_acl("@partner"):
        return redirect(url_for("main.route_dashboard"), 302)

    try:
        db.session.query(ComponentShardInfo).filter(
            ComponentShardInfo.component_shard_info_id == component_shard_info_id
        ).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("shards.route_list"))
    flash("Deleted shard", "info")
    return redirect(url_for("shards.route_list"))


@bp_shards.route("/<int:component_shard_info_id>/details")
@login_required
def route_show(component_shard_info_id: int) -> Any:
    # security check
    if not g.user.check_acl("@partner"):
        return redirect(url_for("main.route_dashboard"), 302)

    # find shard
    try:
        shard = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # show details
    claims = db.session.query(Claim).order_by(Claim.summary).all()
    return render_template(
        "shard-details.html", category="firmware", claims=claims, shard=shard
    )


@bp_shards.route("/<int:component_shard_info_id>/components")
@login_required
def route_components(component_shard_info_id: int) -> Any:
    # find shard
    try:
        shard_info = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # get shards with a unique component appstream ID
    shards = (
        db.session.query(ComponentShard)
        .join(ComponentShardInfo)
        .filter(ComponentShardInfo.component_shard_info_id == component_shard_info_id)
        .join(Component)
        .distinct(Component.appstream_id)
        .order_by(Component.appstream_id, ComponentShard.name.desc())
        .all()
    )
    unique_mds: Dict[str, List[Component]] = defaultdict(list)
    for shard in shards:
        if shard.md not in unique_mds[shard.name]:
            if not shard.md.fw.check_acl("@view"):
                continue
            unique_mds[shard.name].append(shard.md)

    # show details
    return render_template(
        "shard-components.html",
        category="firmware",
        unique_mds=unique_mds,
        shard=shard_info,
    )


@bp_shards.route("/<int:component_shard_info_id>/checksums")
@login_required
@admin_login_required
def route_checksums(component_shard_info_id: int) -> Any:
    # find shard
    try:
        shard_info = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # get shards with a unique component appstream ID
    shards_by_checksum: Dict[str, List[ComponentShard]] = defaultdict(list)
    for shard in (
        db.session.query(ComponentShard)
        .join(ComponentShardInfo)
        .filter(ComponentShardInfo.component_shard_info_id == component_shard_info_id)
    ):
        shards_by_checksum[shard.checksum].append(shard)

    # show details
    return render_template(
        "shard-checksums.html",
        category="firmware",
        shards_by_checksum=shards_by_checksum,
        shard=shard_info,
    )


@bp_shards.route("/<int:component_shard_info_id>/claims")
@login_required
@admin_login_required
def route_claims(component_shard_info_id: int) -> Any:
    # find shard
    try:
        shard = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # get claims for this ComponentShardInfo
    claims = (
        db.session.query(ComponentShardClaim)
        .filter(ComponentShardClaim.component_shard_info_id == component_shard_info_id)
        .all()
    )

    # show details
    claims_all = db.session.query(Claim).order_by(Claim.summary).all()
    return render_template(
        "shard-claims.html",
        category="firmware",
        claims=claims,
        claims_all=claims_all,
        shard=shard,
    )


@bp_shards.route(
    "/<int:component_shard_info_id>/claim/<int:component_shard_claim_id>/delete",
    methods=["POST"],
)
@login_required
@admin_login_required
def route_shard_claim_delete(
    component_shard_info_id: int, component_shard_claim_id: int
) -> Any:
    # find shard
    try:
        db.session.query(ComponentShardClaim).filter(
            ComponentShardClaim.component_shard_info_id == component_shard_info_id
        ).filter(
            ComponentShardClaim.component_shard_claim_id == component_shard_claim_id
        ).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("shards.route_list"))
    flash("Deleted shard claim", "info")
    return redirect(
        url_for("shards.route_claims", component_shard_info_id=component_shard_info_id)
    )


@bp_shards.post("/<int:component_shard_info_id>/claim/create")
@login_required
@admin_login_required
def route_shard_claim_create(component_shard_info_id: int) -> Any:
    # ensure has enough data
    for key in ["checksum", "claim_id"]:
        if key not in request.form:
            flash(f"No {key} form data found!", "warning")
            return redirect(url_for("shards.route_list"))

    # add issue
    try:
        claim = ComponentShardClaim(
            component_shard_info_id=component_shard_info_id,
            checksum=request.form["checksum"],
            claim_id=request.form["claim_id"],
        )
        db.session.add(claim)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash(
            "Failed to add component shard claim: The checksum already exists", "info"
        )
        return redirect(url_for("issues.route_list"))
    flash("Added claim", "info")
    return redirect(
        url_for("shards.route_claims", component_shard_info_id=component_shard_info_id)
    )


@bp_shards.route("/<int:component_shard_id>/download")
@login_required
def route_download(component_shard_id: int) -> Any:
    # find shard
    try:
        shard = (
            db.session.query(ComponentShard)
            .filter(ComponentShard.component_shard_id == component_shard_id)
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))
    if not shard.md.fw.check_acl("@view"):
        flash("Permission denied: Unable to download shard", "danger")
        return redirect(url_for("main.route_dashboard"))
    blob = shard.read()
    if not blob:
        flash("Permission denied: Shard has no data", "warning")
        return redirect(url_for("main.route_dashboard"))
    response = make_response(blob)
    response.headers.set("Content-Type", "application/octet-stream")
    response.headers.set(
        "Content-Disposition",
        "attachment",
        filename=f"{shard.guid}-{shard.checksum}",
    )
    return response


@bp_shards.route("/export/json")
@login_required
def route_export_json() -> Any:
    # security check
    if not g.user.check_acl("@partner"):
        return redirect(url_for("main.route_dashboard"), 302)

    # build data
    items: List[Any] = []
    for info in db.session.query(ComponentShardInfo):
        item: Dict[str, Any] = {}
        item["Id"] = info.component_shard_info_id
        item["Guid"] = info.guid
        if info.cnt:
            item["Cnt"] = info.cnt
        if info.name:
            item["Name"] = info.name
        if info.description:
            item["Description"] = info.description
        appstream_ids: List[str] = []
        for shard in info.shards:
            if shard.name not in appstream_ids:
                appstream_ids.append(shard.name)
        if appstream_ids:
            item["AppstreamIDs"] = appstream_ids
        items.append(item)

    # format it as JSON
    response = make_response(json.dumps(items, indent=4, separators=(",", ": ")))
    response.headers.set("Content-Type", "application/json")
    response.headers.set(
        "Content-Disposition",
        "attachment",
        filename="dump.json",
    )
    return response


@bp_shards.route("/export/csv")
@cache.cached()
def route_export_csv() -> Any:
    # write to a CSV file with no header
    f = StringIO()
    spamwriter = csv.writer(f)
    for info in (
        db.session.query(ComponentShardInfo)
        .filter(ComponentShardInfo.name.startswith("org.uefi."))
        .order_by(ComponentShardInfo.guid.asc())
    ):
        spamwriter.writerow([info.guid.upper(), info.name.rsplit(".")[-1]])

    # format it as JSON
    response = make_response(f.getvalue())
    response.headers.set("Content-Type", "text/csv")
    response.headers.set(
        "Content-Disposition",
        "attachment",
        filename="guids.csv",
    )
    return response


@bp_shards.post("/import/csv")
@login_required
def route_import_csv() -> Any:
    # security check
    if not g.user.check_acl("@admin"):
        flash("Permission denied: Insufficient permissions", "danger")
        return redirect(url_for("main.route_dashboard"), 302)

    # read file
    try:
        fileitem = request.files["file"]
    except KeyError:
        flash("Failed to add asset: No file", "warning")
        return redirect(url_for("shards.route_list"), 302)
    if not fileitem.filename:
        flash("Failed to add asset: No filename", "warning")
        return redirect(url_for("shards.route_list"), 302)
    blob = fileitem.read()

    # read CSV file
    guid_names: Dict[str, str] = {}
    try:
        reader = csv.reader(StringIO(blob.decode(errors="ignore")))
    except ValueError:
        flash("No CSV object could be decoded", "warning")
        return redirect(url_for("shards.route_list"), 302)
    for row in reader:
        try:
            if row[0].startswith("#"):
                continue
            guid: str = row[0].lower()
            if not _validate_guid(guid):
                continue
            name: str = row[1]
            if len(name.split(".")) < 3:
                name = f"org.uefi.Unknown.{name}"
            guid_names[guid] = name
        except (IndexError, ValueError):
            pass

    # fix up our database
    cnt_added: int = 0
    cnt_modified: int = 0
    for guid, name in guid_names.items():
        try:
            info = (
                db.session.query(ComponentShardInfo)
                .filter(ComponentShardInfo.guid == guid)
                .one()
            )
            if not info.name:
                info.name = name
                cnt_modified += 1
        except NoResultFound:
            info = ComponentShardInfo(guid=guid, name=name)
            cnt_added += 1
            db.session.add(info)
    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to import file", "info")
        return redirect(url_for("shards.route_list"))

    # success
    flash(f"Imported file, added {cnt_added} and modified {cnt_modified} items", "info")
    return redirect(url_for("shards.route_list"))
